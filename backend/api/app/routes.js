module.exports = function (app, passport, fs, nodemailer, request) {

  //var backend_path =  './../../backend/src/assets/page/'; 	
  //var frontend_path = './../../frontend/src/assets/page/';

  var backend_path =  '/var/www/html/sharkpads/backend/assets/page/'; 	
  var frontend_path = '/var/www/html/sharkpads/frontend/assets/page/';


  app.post('/api_reservations',  function (req, res) {
      console.log(req.body,'body');

      let req_url = 'https://api.guesty.com/api/v2/reservations/create';
      var options = { method: 'POST',
      url: 'https://api.guesty.com/api/v2/reservations/create',
      headers: 
       { 'postman-token': 'cceca89f-3e5f-73d4-76a6-a55cf76a4b93',
         'cache-control': 'no-cache',
         'content-type': 'application/json',
         authorization: 'Basic NmM3YTM1ZmNkM2UwN2Y2MWJlMzQzMjczZWNjMWNhMmU6NWE3YjZiNjg1YzRiNDA1OGUxZTBmOGVmNWY0ZGRhZGM=' },
      body: 
       { reservation: 
          { 
            listingId: req.body.id,
            checkInDateLocalized: req.body.from,
            checkOutDateLocalized: req.body.to,
            status: 'inquiry',
            guestsCount: req.body.guests,
            source: "website"
          },
          money: {
            "fareAccommodation": req.body.price,
            "currency": "USD"
          },
          guest: {
            firstName: req.body.firstname,
            lastName: req.body.lastname,
            phone: req.body.phone,
            email: req.body.email,
          }
       },
      json: true };

      request(options, function (error, response, body) {
        if (error){
          res.status(200).send({ status:false,msg:'There is an error in api, try again'});
        }else{
          //console.log('test')
          console.log('test',error)
          console.log('response',response)
          //console.log('body',JSON.parse(body))
          //console.log(body,'----------')
          //var testIfJson = JSON.parse(body);
          if (body && typeof body.valueOf() !== "string") {
            res.status(200).send({ status:true,msg:'success',response:body});
          }else{
            res.status(200).send({ status:false,msg:'success',response:body});
          }
        }
      });
  });

  app.post('/api_property_quote',  function (req, res) {
      console.log(req.body,'body');

      let guests = req.body.guests;
      if(req.body.guests=='Guests' || req.body.guests=='guests'){
        guests = 1;
      }

      let req_url = 'https://api.guesty.com/api/v2/reservations/quote';
      var options = { method: 'GET',
      url: req_url,
      qs:{ listingId: req.body.id,
       checkIn: req.body.from,
       checkOut: req.body.to,
       guestsCount: guests,
       channelId: 'manual',
       source: 'website' 
      },
      headers: 
       { 'postman-token': '9c9b675f-0683-3e16-d0c4-72466aef7d46',
         'cache-control': 'no-cache',
         authorization: 'Basic NmM3YTM1ZmNkM2UwN2Y2MWJlMzQzMjczZWNjMWNhMmU6NWE3YjZiNjg1YzRiNDA1OGUxZTBmOGVmNWY0ZGRhZGM=' },
      form: {} };

      request(options, function (error, response, body) {
        if (error){
          res.status(200).send({ status:false,msg:'There is an error in api, try again'});
        }else{
          //console.log('test')
          //console.log(body,'----------')
          res.status(200).send({ status:true,msg:'success',response:JSON.parse(body)});
        }
      });
  });

  app.post('/api_property',  function (req, res) {
      console.log(req.body,'body');

      let req_url = 'https://api.guesty.com/api/v2/listings/'+req.body.id;
      var options = { method: 'GET',
      url: req_url,
      headers: 
       { 'postman-token': '9c9b675f-0683-3e16-d0c4-72466aef7d46',
         'cache-control': 'no-cache',
         authorization: 'Basic NmM3YTM1ZmNkM2UwN2Y2MWJlMzQzMjczZWNjMWNhMmU6NWE3YjZiNjg1YzRiNDA1OGUxZTBmOGVmNWY0ZGRhZGM=' },
      form: {} };

      request(options, function (error, response, body) {
        if (error){
          res.status(200).send({ status:false,msg:'There is an error in api, try again'});
        }else{
          //console.log('test')
          //console.log(body,'----------')
          res.status(200).send({ status:true,msg:'success',response:JSON.parse(body)});
        }
      });
  });

  app.post('/api_guesty',  function (req, res) {
      console.log(req.body,'bodyddddddddddd');

      let guests = req.body.guests;
      if(req.body.guests=='Guests' || req.body.guests=='guests'){
        guests = 1;
      }

      var options = { method: 'GET',
      url: 'https://api.guesty.com/api/v2/listings',
      qs: 
      { available: '{"checkIn":"'+req.body.from+'","checkOut":"'+req.body.to+'","minOccupancy":"'+guests+'"}',
      listed: 'true',
      active: 'true', 
      },
      headers: 
       { 'postman-token': '9c9b675f-0683-3e16-d0c4-72466aef7d46',
         'cache-control': 'no-cache',
         authorization: 'Basic NmM3YTM1ZmNkM2UwN2Y2MWJlMzQzMjczZWNjMWNhMmU6NWE3YjZiNjg1YzRiNDA1OGUxZTBmOGVmNWY0ZGRhZGM=' },
      form: {} };

      console.log(options,'options')

      request(options, function (error, response, body) {
        if (error){
          res.status(200).send({ status:false,msg:'There is an error in api, try again'});
        }else{
          //console.log('test')
          //console.log(body.results,'----------')
          res.status(200).send({ status:true,msg:'success',response:JSON.parse(body)});
        }
      });
  });

   app.post('/api_contact_us', async  function (req, res) {
      console.log(req.body,'body');

<<<<<<< HEAD
      
      var transporter = nodemailer.createTransport({
        host: 'smtp.office365.com',
        port: 587,
        //secure: true, // use SSL
        auth: {
          user: 'hello@sharkpads.com',
          pass: 'SharkPadHello2020'
        }
      });
      

      /*
=======
>>>>>>> 8984940b6fd992eef6756565fbc19ce2057b00d1
      var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: 'sanjaygora@cwebconsultants.com',
          pass: 'sanjay@123'
        }
      });
<<<<<<< HEAD
      */
=======
>>>>>>> 8984940b6fd992eef6756565fbc19ce2057b00d1

      let msg = '';
      if(req.body.phone){
        msg = req.body.name + ' has sent you email to contact' + 'Here is phone number of customer' + ' ' + req.body.phone + ' ' + req.body.comment;
      }else{
        msg = req.body.name + ' has sent you email to contact' + ' ' + req.body.comment;
      }

     msg = '';
     msg += 'Customer Name: '+req.body.name + '<br><br>';  
     msg += 'Customer Email: '+req.body.email + '<br><br>';  
     msg += 'Customer Phone: '+req.body.phone + '<br><br>';  
     msg += 'Customer Comments: '+req.body.comment;  

     let email_data = '';
     // results = await db.query('Select * from settings limit 1'); 
     // console.log(JSON.stringify(results),'results');
      
      db.query('Select * from settings limit 1', await function(err,result){
        if (err) {  
          //res.status(200).send({ page: result,error:err });
        }else{

          results=JSON.parse(JSON.stringify(result));
          email_data = results[0].inquire_email;
          console.log(email_data,'email_data')

          var mailOptions = {
            from: 'hello@sharkpads.com',
            to: email_data,
<<<<<<< HEAD
            subject: req.body.subject,
=======
            subject: 'Inquire Form Query',
>>>>>>> 8984940b6fd992eef6756565fbc19ce2057b00d1
            html: msg
          };

          transporter.sendMail(mailOptions, function(error, info){
            
            console.log(error,'error',info,'info');

            if (error) {
              res.status(200).send({ status:false,msg:'Mail sent failed' });
            } else {
              res.status(200).send({ status:true,msg:'Mail sent successfully' });
            }
          });
        }
      }); 
  });  

  app.post('/api_homepage',  function (req, res) {
      var file_name = req.query.uploads;

      if(req.files){
        let sampleFile = req.files.uploads;
        let uni = new Date().getTime();
        file_name = uni+'_'+req.files.uploads.name.replace(/ /g,"_");
        sampleFile.mv(backend_path+file_name);
        sampleFile.mv(frontend_path+file_name);

        db.query('Select * from hompeage_content', function(err,result){
          if (err) {  
            res.status(200).send({ msg:err.sqlMessage });
          }else{
            console.log(result,'result');

            if(result.length > 0){

              if (result[0].homepage_video && fs.existsSync(backend_path+result[0].homepage_video)) {
                fs.unlinkSync(backend_path+result[0].homepage_video,function(err){
                  console.log(err);
                }); 
              }

              if (result[0].homepage_video && fs.existsSync(frontend_path+result[0].homepage_video)) {
                fs.unlinkSync(frontend_path+result[0].homepage_video,function(err){
                  console.log(err);
                }); 
              }

              results=JSON.parse(JSON.stringify(result));
              
              var sql = "UPDATE `hompeage_content` set homepage_video =?  WHERE id = ?";
              db.query(sql,[file_name,results[0].id], (err, result) => {
                 
                 console.log(err,'err');

               if (err) {
                 res.status(200).send({ status: false,msg:err.sqlMessage });
               }else{
                 res.status(200).send({ msg: 'file uploaded successfully',name:file_name}); 
               }
             });
            }else{
              let query = "INSERT INTO `hompeage_content`  (page_id,homepage_video) VALUES ('" + req.body.page_id + "','" +
                  file_name + "')";
                db.query(query, (err, result) => {
                if (err) {
                  res.status(200).send({ msg:err.sqlMessage });
                }else{
                  res.status(200).send({ msg: 'file uploaded successfully',name:file_name}); 
                }
              });
            }
          }
        });
        }else{
          res.status(200).send({ msg: 'no file uploaded'});
      } 
  });

  app.post('/api_travel_design',  function (req, res) {
      var content_one = req.body.content_one;
      var content_two = req.body.content_two;
      var content_three = req.body.content_three;

      if(req.files){
        console.log(req.files.pic_one,'files');
        let uni = new Date().getTime();
        
        file_name = '';
        if(req.files.uploads){
          let sampleFile = req.files.uploads;
          file_name = uni+'_1_'+req.files.uploads.name.replace(/ /g,"_");
          sampleFile.mv(backend_path+file_name);
          sampleFile.mv(frontend_path+file_name);
        }

        file_name1 = '';
        if(req.files.pic_one){
          let sampleFile1 = req.files.pic_one;
          file_name1 = uni+'_2_'+req.files.pic_one.name.replace(/ /g,"_");
          sampleFile1.mv(backend_path+file_name1); 
          sampleFile1.mv(frontend_path+file_name1); 
        }

        file_name2 = '';
        if(req.files.pic_two){
          let sampleFile2 = req.files.pic_two;
          file_name2 = uni+'_3_'+req.files.pic_two.name.replace(/ /g,"_");
          sampleFile2.mv(backend_path+file_name2);
          sampleFile2.mv(frontend_path+file_name2);
<<<<<<< HEAD
        }

        file_name3 = '';
        if(req.files.pic_three){
          let sampleFile3 = req.files.pic_three;
          file_name3 = uni+'_4_'+req.files.pic_three.name.replace(/ /g,"_");
          sampleFile3.mv(backend_path+file_name3);
          sampleFile3.mv(frontend_path+file_name3);
=======
>>>>>>> 8984940b6fd992eef6756565fbc19ce2057b00d1
        }

        db.query('Select * from traveldesign', function(err,result){
          if (err) {  
            res.status(200).send({ msg:err.sqlMessage });
          }else{
            console.log(result,'result');

            if(result.length > 0){

              if(req.files.uploads){
                if (result[0].header_video && fs.existsSync(backend_path+result[0].header_video)) {
                  fs.unlinkSync(backend_path+result[0].header_video,function(err){
                    console.log(err);
                  }); 
                }

                if (result[0].header_video && fs.existsSync(frontend_path+result[0].header_video)) {
                  fs.unlinkSync(frontend_path+result[0].header_video,function(err){
                    console.log(err);
                  }); 
                }
              }else{
                file_name = result[0].header_video;
              }

              if(req.files.pic_one){
                if (result[0].pic_one && fs.existsSync(backend_path+result[0].pic_one)) {
                  fs.unlinkSync(backend_path+result[0].pic_one,function(err){
                    console.log(err);
                  }); 
                }

                if (result[0].pic_one && fs.existsSync(frontend_path+result[0].pic_one)) {
                  fs.unlinkSync(frontend_path+result[0].pic_one,function(err){
                    console.log(err);
                  }); 
                }
              }else{
                  file_name1 = result[0].pic_one;
              }

              if(req.files.pic_two){
                if (result[0].pic_two && fs.existsSync(backend_path+result[0].pic_two)) {
                  fs.unlinkSync(backend_path+result[0].pic_two,function(err){
                    console.log(err);
                  }); 
                }

                if (result[0].pic_two && fs.existsSync(frontend_path+result[0].pic_two)) {
                  fs.unlinkSync(frontend_path+result[0].pic_two,function(err){
                    console.log(err);
                  }); 
                }
              }else{
                  file_name2 = result[0].pic_two;
              }

              if(req.files.pic_three){
                if (result[0].pic_three && fs.existsSync(backend_path+result[0].pic_three)) {
                  fs.unlinkSync(backend_path+result[0].pic_three,function(err){
                    console.log(err);
                  }); 
                }

                if (result[0].pic_three && fs.existsSync(frontend_path+result[0].pic_three)) {
                  fs.unlinkSync(frontend_path+result[0].pic_three,function(err){
                    console.log(err);
                  }); 
                }
              }else{
                  file_name3 = result[0].pic_three;
              }

              results=JSON.parse(JSON.stringify(result));
              
              var sql = "UPDATE `traveldesign` set header_video =?,pic_one =?,pic_two =?,pic_three =?,content_one =?,content_two =?,content_three =?  WHERE id = ?";
              db.query(sql,[file_name,file_name1,file_name2,file_name3,content_one,content_two,content_three,results[0].id], (err, result) => {
               if (err) {
                 res.status(200).send({ status: false,msg:err.sqlMessage });
               }else{
                 res.status(200).send({ msg: 'Page updated successfully'}); 
               }
             });
            }else{
              let query = "INSERT INTO `traveldesign` (page_id,header_video,pic_one,pic_two,pic_three,content_one,content_two,content_three) VALUES ('" + req.body.page_id + "','" +
                  file_name + "','" + file_name1 + "','" + file_name2 + "','" + file_name3 + "','" + content_one + "','" + content_two + "','" + content_three + "')";
                db.query(query, (err, result) => {
                if (err) {
                  res.status(200).send({ msg:err.sqlMessage });
                }else{
                  res.status(200).send({ msg: 'Page updated successfully'}); 
                }
              });
            }
          }
        });
        }else{
          db.query('Select * from traveldesign', function(err,result){
          if (err) {  
              res.status(200).send({ msg:err.sqlMessage });
          }else{
              
              if(result.length > 0){
                var sql = "UPDATE `traveldesign` set content_one =?,content_two =?,content_three =?  WHERE id = ?";
                db.query(sql,[content_one,content_two,content_three,results[0].id], (err, result) => {
                 if (err) {
                   res.status(200).send({ status: false,msg:err.sqlMessage });
                 }else{
                   res.status(200).send({ msg: 'Page updated successfully'}); 
                 }
               });
              }else{
                let query = "INSERT INTO `traveldesign` (page_id,content_one,content_two,content_three) VALUES ('" + req.body.page_id + "','" + content_one + "','" + content_two + "','" + content_three + "')";
                db.query(query, (err, result) => {
                if (err) {
                  res.status(200).send({ msg:err.sqlMessage });
                }else{
                  res.status(200).send({ msg: 'Page updated successfully'}); 
                }
              });
              }
            } 
          });  
      } 
  });

  app.post('/api_life_style',  function (req, res) {
      var content_one = req.body.content_one;
      var content_two = req.body.content_two;
      var content_three = req.body.content_three;
      var content_four = req.body.content_four;

      if(req.files){
        console.log(req.files.pic_one,'files');

        let uni = new Date().getTime();
        
        file_name = '';
        if(req.files.uploads){
          let sampleFile = req.files.uploads;
          file_name = uni+'_1_'+req.files.uploads.name.replace(/ /g,"_");
          sampleFile.mv(backend_path+file_name);
          sampleFile.mv(frontend_path+file_name);
        }

        file_name1 = '';
        if(req.files.pic_one){
          let sampleFile1 = req.files.pic_one;
          file_name1 = uni+'_2_'+req.files.pic_one.name.replace(/ /g,"_");
          sampleFile1.mv(backend_path+file_name1); 
          sampleFile1.mv(frontend_path+file_name1); 
        }

        file_name2 = '';
        if(req.files.pic_two){
          let sampleFile2 = req.files.pic_two;
          file_name2 = uni+'_3_'+req.files.pic_two.name.replace(/ /g,"_");
          sampleFile2.mv(backend_path+file_name2);
          sampleFile2.mv(frontend_path+file_name2);
        }

        file_name3 = '';
        if(req.files.pic_three){
          let sampleFile3 = req.files.pic_three;
          
          console.log(sampleFile3,'sampleFile3')  

          file_name3 = uni+'_4_'+req.files.pic_three.name.replace(/ /g,"_");
          sampleFile3.mv(backend_path+file_name3);
          sampleFile3.mv(frontend_path+file_name3);
        }

        db.query('Select * from life_style', function(err,result){
          if (err) {  
            res.status(200).send({ msg:err.sqlMessage });
          }else{
            console.log(result,'result');

            if(result.length > 0){

              if(req.files.uploads){
                if (result[0].header_video && fs.existsSync(backend_path+result[0].header_video)) {
                  fs.unlinkSync(backend_path+result[0].header_video,function(err){
                    console.log(err);
                  }); 
                }

                if (result[0].header_video && fs.existsSync(frontend_path+result[0].header_video)) {
                  fs.unlinkSync(frontend_path+result[0].header_video,function(err){
                    console.log(err);
                  }); 
                }
              }else{
                file_name = result[0].header_video;
              }

              if(req.files.pic_one){
                if (result[0].pic_one && fs.existsSync(backend_path+result[0].pic_one)) {
                  fs.unlinkSync(backend_path+result[0].pic_one,function(err){
                    console.log(err);
                  }); 
                }

                if (result[0].pic_one && fs.existsSync(frontend_path+result[0].pic_one)) {
                  fs.unlinkSync(frontend_path+result[0].pic_one,function(err){
                    console.log(err);
                  }); 
                }
              }else{
                  file_name1 = result[0].pic_one;
              }

              if(req.files.pic_two){
                if (result[0].pic_two && fs.existsSync(backend_path+result[0].pic_two)) {
                  fs.unlinkSync(backend_path+result[0].pic_two,function(err){
                    console.log(err);
                  }); 
                }

                if (result[0].pic_two && fs.existsSync(frontend_path+result[0].pic_two)) {
                  fs.unlinkSync(frontend_path+result[0].pic_two,function(err){
                    console.log(err);
                  }); 
                }
              }else{
                  file_name2 = result[0].pic_two;
              }

              if(req.files.pic_three){
                if (result[0].pic_three && fs.existsSync(backend_path+result[0].pic_three)) {
                  fs.unlinkSync(backend_path+result[0].pic_three,function(err){
                    console.log(err);
                  }); 
                }

                if (result[0].pic_three && fs.existsSync(frontend_path+result[0].pic_three)) {
                  fs.unlinkSync(frontend_path+result[0].pic_three,function(err){
                    console.log(err);
                  }); 
                }
              }else{
                  file_name3 = result[0].pic_three;
              }

              results=JSON.parse(JSON.stringify(result));
              
              var sql = "UPDATE `life_style` set header_video =?,pic_one =?,pic_two =?,pic_three =?,content_one =?,content_two =?,content_three =?,content_four =? WHERE id = ?";
              db.query(sql,[file_name,file_name1,file_name2,file_name3,content_one,content_two,content_three,content_four,results[0].id], (err, result) => {
                console.log('err',err)
               if (err) {
                 res.status(200).send({ status: false,msg:err.sqlMessage });
               }else{
                 res.status(200).send({ msg: 'Page updated successfully'}); 
               }
             });
            }else{
              let query = "INSERT INTO `life_style` (page_id,header_video,pic_one,pic_two,pic_three,content_one,content_two,content_three,content_four) VALUES ('" + req.body.page_id + "','" +
                  file_name + "','" + file_name1 + "','" + file_name2 + "','" + file_name3 + "','" + content_one + "','" + content_two + "','" + content_three + "','" + content_four + "')";
                db.query(query, (err, result) => {
                if (err) {
                  res.status(200).send({ msg:err.sqlMessage });
                }else{
                  res.status(200).send({ msg: 'Page updated successfully'}); 
                }
              });
            }
          }
        });
        }else{
          db.query('Select * from life_style', function(err,result){
          if (err) {  
              res.status(200).send({ msg:err.sqlMessage });
          }else{
              
              if(result.length > 0){
                var sql = "UPDATE `life_style` set content_one =?,content_two =?,content_three =?,content_four =?  WHERE id = ?";
                db.query(sql,[content_one,content_two,content_three,content_four,results[0].id], (err, result) => {
                 if (err) {
                   res.status(200).send({ status: false,msg:err.sqlMessage });
                 }else{
                   res.status(200).send({ msg: 'Page updated successfully'}); 
                 }
               });
              }else{
                let query = "INSERT INTO `life_style` (page_id,content_one,content_two,content_three,content_four) VALUES ('" + req.body.page_id + "','" + content_one + "','" + content_two + "','" + content_three + "','" + content_four + "')";
                db.query(query, (err, result) => {
                if (err) {
                  res.status(200).send({ msg:err.sqlMessage });
                }else{
                  res.status(200).send({ msg: 'Page updated successfully'}); 
                }
              });
              }
            } 
          });  
      } 
  });

  app.post('/api_rentals',  function (req, res) {
      var content_one = req.body.content_one;
      var content_two = req.body.content_two;
      var content_three = req.body.content_three;

      if(req.files){
        console.log(req.files.pic_one,'files');

        let uni = new Date().getTime();
        
        file_name = '';
        if(req.files.uploads){
          let sampleFile = req.files.uploads;
          file_name = uni+'_1_'+req.files.uploads.name.replace(/ /g,"_");
          sampleFile.mv(backend_path+file_name);
          sampleFile.mv(frontend_path+file_name);
        }

        file_name1 = '';
        if(req.files.pic_one){
          let sampleFile1 = req.files.pic_one;
          file_name1 = uni+'_2_'+req.files.pic_one.name.replace(/ /g,"_");
          sampleFile1.mv(backend_path+file_name1); 
          sampleFile1.mv(frontend_path+file_name1); 
        }

        file_name2 = '';
        if(req.files.pic_two){
          let sampleFile2 = req.files.pic_two;
          file_name2 = uni+'_3_'+req.files.pic_two.name.replace(/ /g,"_");
          sampleFile2.mv(backend_path+file_name2);
          sampleFile2.mv(frontend_path+file_name2);
        }

        db.query('Select * from rentals', function(err,result){
          if (err) {  
            res.status(200).send({ msg:err.sqlMessage });
          }else{
            console.log(result,'result');

            if(result.length > 0){

              if(req.files.uploads){
                if (result[0].header_video && fs.existsSync(backend_path+result[0].header_video)) {
                  fs.unlinkSync(backend_path+result[0].header_video,function(err){
                    console.log(err);
                  }); 
                }

                if (result[0].header_video && fs.existsSync(frontend_path+result[0].header_video)) {
                  fs.unlinkSync(frontend_path+result[0].header_video,function(err){
                    console.log(err);
                  }); 
                }
              }else{
                file_name = result[0].header_video;
              }

              if(req.files.pic_one){
                if (result[0].pic_one && fs.existsSync(backend_path+result[0].pic_one)) {
                  fs.unlinkSync(backend_path+result[0].pic_one,function(err){
                    console.log(err);
                  }); 
                }

                if (result[0].pic_one && fs.existsSync(frontend_path+result[0].pic_one)) {
                  fs.unlinkSync(frontend_path+result[0].pic_one,function(err){
                    console.log(err);
                  }); 
                }
              }else{
                  file_name1 = result[0].pic_one;
              }

              if(req.files.pic_two){
                if (result[0].pic_two && fs.existsSync(backend_path+result[0].pic_two)) {
                  fs.unlinkSync(backend_path+result[0].pic_two,function(err){
                    console.log(err);
                  }); 
                }

                if (result[0].pic_two && fs.existsSync(frontend_path+result[0].pic_two)) {
                  fs.unlinkSync(frontend_path+result[0].pic_two,function(err){
                    console.log(err);
                  }); 
                }
              }else{
                  file_name2 = result[0].pic_two;
              }

              

              results=JSON.parse(JSON.stringify(result));
              
              var sql = "UPDATE `rentals` set header_video =?,pic_one =?,pic_two =?,content_one =?,content_two =?,content_three=? WHERE id = ?";
              db.query(sql,[file_name,file_name1,file_name2,content_one,content_two,content_three,results[0].id], (err, result) => {
                console.log('err',err)
               if (err) {
                 res.status(200).send({ status: false,msg:err.sqlMessage });
               }else{
                 res.status(200).send({ msg: 'Page updated successfully'}); 
               }
             });
            }else{
              let query = "INSERT INTO `rentals` (page_id,header_video,pic_one,pic_two,content_one,content_two,content_three) VALUES ('" + req.body.page_id + "','" +
                  file_name + "','" + file_name1 + "','" + file_name2 + "','" + content_one + "','" + content_two + "','" + content_three + "')";
                db.query(query, (err, result) => {
                if (err) {
                  res.status(200).send({ msg:err.sqlMessage });
                }else{
                  res.status(200).send({ msg: 'Page updated successfully'}); 
                }
              });
            }
          }
        });
        }else{
          db.query('Select * from rentals', function(err,result){
          if (err) {  
              res.status(200).send({ msg:err.sqlMessage });
          }else{
              
              if(result.length > 0){
                var sql = "UPDATE `rentals` set content_one =?,content_two =?,content_three =?  WHERE id = ?";
                db.query(sql,[content_one,content_two,content_three,results[0].id], (err, result) => {
                 if (err) {
                   res.status(200).send({ status: false,msg:err.sqlMessage });
                 }else{
                   res.status(200).send({ msg: 'Page updated successfully'}); 
                 }
               });
              }else{
                let query = "INSERT INTO `rentals` (page_id,content_one,content_two,content_three) VALUES ('" + req.body.page_id + "','" + content_one + "','" + content_two + "','" + content_three + "')";
                db.query(query, (err, result) => {
                if (err) {
                  res.status(200).send({ msg:err.sqlMessage });
                }else{
                  res.status(200).send({ msg: 'Page updated successfully'}); 
                }
              });
              }
            } 
          });  
      } 
  });

  app.post('/api_settings',  function (req, res) {
      var amazon = req.body.amazon;
      var facebook = req.body.facebook;
      var twitter = req.body.twitter;
      var instgram = req.body.instgram;
      var linkedin = req.body.linkedin;
      var tiktok = req.body.tiktok;
      var airnb = req.body.airnb;

      var phone = req.body.phone;
      var email = req.body.email;
      var address = req.body.address;
      var common_content = req.body.common_content;

      var map = req.body.map;
      var aboutus = req.body.aboutus;
      var inquire_email = req.body.inquire_email;
<<<<<<< HEAD
      var gallery_text = req.body.gallery_text;
=======
>>>>>>> 8984940b6fd992eef6756565fbc19ce2057b00d1

      if(req.files){
        let uni = new Date().getTime();

        file_name = '';
        if(req.files.uploads){
          let sampleFile = req.files.uploads;
          file_name = uni+'_1_'+req.files.uploads.name.replace(/ /g,"_");
          sampleFile.mv(backend_path+file_name);
          sampleFile.mv(frontend_path+file_name);
        }
      }  

      db.query('Select * from settings limit 1', function(err,result){
          if (err) {  
            res.status(200).send({ msg:err.sqlMessage });
          }else{
            if(result.length > 0){

                if(req.files){
                  if (result[0].pic_gallery_image && fs.existsSync(backend_path+result[0].pic_gallery_image)) {
                    fs.unlinkSync(backend_path+result[0].pic_gallery_image,function(err){
                      console.log(err);
                    }); 
                  }

                  if (result[0].pic_gallery_image && fs.existsSync(frontend_path+result[0].pic_gallery_image)) {
                    fs.unlinkSync(frontend_path+result[0].pic_gallery_image,function(err){
                      console.log(err);
                    }); 
                  }
                }else{
                  file_name = result[0].pic_gallery_image;
                }

<<<<<<< HEAD
                var sql = "UPDATE `settings` set amazon=?,gallery_text=?,airnb=?,tiktok =?,facebook =?,twitter =?,instgram =?,linkedin =?,phone =?,email =?,address =?,common_content =?,map =?,aboutus =?,pic_gallery_image=?,inquire_email=? WHERE id = ?";
                db.query(sql,[amazon,gallery_text,airnb,tiktok,facebook,twitter,instgram,linkedin,phone,email,address,common_content,map,aboutus,file_name,inquire_email,result[0].id], (err, result) => {
=======
                var sql = "UPDATE `settings` set airnb=?,tiktok =?,facebook =?,twitter =?,instgram =?,linkedin =?,phone =?,email =?,address =?,common_content =?,map =?,aboutus =?,pic_gallery_image=?,inquire_email=? WHERE id = ?";
                db.query(sql,[airnb,tiktok,facebook,twitter,instgram,linkedin,phone,email,address,common_content,map,aboutus,file_name,inquire_email,result[0].id], (err, result) => {
>>>>>>> 8984940b6fd992eef6756565fbc19ce2057b00d1
                 if (err) {
                   res.status(200).send({ status: false,msg:err.sqlMessage });
                 }else{
                   res.status(200).send({ msg: 'Settings updated successfully'}); 
                 }
               });
              }else{
<<<<<<< HEAD
                let query = "INSERT INTO `settings` (amazon, gallery_text,airnb,tiktok,facebook,twitter,instgram,linkedin,phone,email,address,common_content,map,aboutus,file_name,pic_gallery_image,inquire_email) VALUES ('" + amazon + "','" + gallery_text + "','" + airnb + "','" + tittok + "','" + facebook + "','" + twitter + "','" + instgram + "','" + linkedin + "','" + phone + "','" + email + "','" + address + "','" + common_content + "','" + map + "','" + aboutus + "','" + file_name + "','" + inquire_email + "')";
=======
                let query = "INSERT INTO `settings` (airnb,tiktok,facebook,twitter,instgram,linkedin,phone,email,address,common_content,map,aboutus,file_name,pic_gallery_image,inquire_email) VALUES ('" + airnb + "','" + tittok + "','" + facebook + "','" + twitter + "','" + instgram + "','" + linkedin + "','" + phone + "','" + email + "','" + address + "','" + common_content + "','" + map + "','" + aboutus + "','" + file_name + "','" + inquire_email + "')";
>>>>>>> 8984940b6fd992eef6756565fbc19ce2057b00d1
                db.query(query, (err, result) => {
                if (err) {
                  res.status(200).send({ msg:err.sqlMessage });
                }else{
                  res.status(200).send({ msg: 'Settings updated successfully'}); 
                }
              });
            }
          }
      }); 
  });    

  app.post('/api_real_estate',  function (req, res) {
      var content_one = req.body.content_one;
      var sharkpads_way_text = req.body.sharkpads_way_text;
      var personal_portfolio_text = req.body.personal_portfolio_text;
      var technology_text = req.body.technology_text;
      var community_relations_text = req.body.community_relations_text;
      var identification_text = req.body.identification_text;
      var contract_and_negotitations_text = req.body.contract_and_negotitations_text;
      var industry_insight_text = req.body.industry_insight_text;
      var partner_text = req.body.partner_text;
      var security_text = req.body.security_text;
      var revenue_management_reporting_text = req.body.revenue_management_reporting_text;
      var marketing_and_branding_text = req.body.marketing_and_branding_text;
      var page_title = req.body.page_title;

      console.log(sharkpads_way_text,'text');

      if(req.files){
        console.log(req.files,'files');
        let uni = new Date().getTime();

        uploads = '';
        if(req.files.uploads){
          let sampleFile_uploads = req.files.uploads;
          uploads = uni+'_12_'+req.files.uploads.name.replace(/ /g,"_");
          sampleFile_uploads.mv(backend_path+uploads);
          sampleFile_uploads.mv(frontend_path+uploads);
        }
        
        file_name = '';
        if(req.files.pic_one){
          let sampleFile = req.files.pic_one;
          file_name = uni+'_1_'+req.files.pic_one.name.replace(/ /g,"_");
          sampleFile.mv(backend_path+file_name);
          sampleFile.mv(frontend_path+file_name);
        }

        file_name1 = '';
        if(req.files.pic_two){
          let sampleFile1 = req.files.pic_two;
          file_name1 = uni+'_2_'+req.files.pic_two.name.replace(/ /g,"_");
          sampleFile1.mv(backend_path+file_name1); 
          sampleFile1.mv(frontend_path+file_name1); 
        }

        file_name2 = '';
        if(req.files.pic_three){
          let sampleFile2 = req.files.pic_three;
          file_name2 = uni+'_3_'+req.files.pic_three.name.replace(/ /g,"_");
          sampleFile2.mv(backend_path+file_name2);
          sampleFile2.mv(frontend_path+file_name2);
<<<<<<< HEAD
        }

        personal_portfolio_name = '';
        if(req.files.personal_portfolio){
          let personal_portfolio = req.files.personal_portfolio;
          personal_portfolio_name = uni+'_3_'+req.files.personal_portfolio.name.replace(/ /g,"_");
          personal_portfolio.mv(backend_path+personal_portfolio_name);
          personal_portfolio.mv(frontend_path+personal_portfolio_name);
        }

        technology_name = '';
        if(req.files.technology){
          let technology = req.files.technology;
          technology_name = uni+'_3_'+req.files.technology.name.replace(/ /g,"_");
          technology.mv(backend_path+technology_name);
          technology.mv(frontend_path+technology_name);
        }

        community_relations_name = '';
        if(req.files.community_relations){
          let community_relations = req.files.community_relations;
          community_relations_name = uni+'_3_'+req.files.community_relations.name.replace(/ /g,"_");
          community_relations.mv(backend_path+community_relations_name);
          community_relations.mv(frontend_path+community_relations_name);
        }

        identification_name = '';
        if(req.files.identification){
          let identification = req.files.identification;
          identification_name = uni+'_3_'+req.files.identification.name.replace(/ /g,"_");
          identification.mv(backend_path+identification_name);
          identification.mv(frontend_path+identification_name);
=======
>>>>>>> 8984940b6fd992eef6756565fbc19ce2057b00d1
        }

        contract_and_negotitations_name = '';
        if(req.files.contract_and_negotitations){
          let contract_and_negotitations = req.files.contract_and_negotitations;
          contract_and_negotitations_name = uni+'_3_'+req.files.contract_and_negotitations.name.replace(/ /g,"_");
          contract_and_negotitations.mv(backend_path+contract_and_negotitations_name);
          contract_and_negotitations.mv(frontend_path+contract_and_negotitations_name);
        }

        industry_insight_name = '';
        if(req.files.industry_insight){
          let industry_insight = req.files.industry_insight;
          industry_insight_name = uni+'_3_'+req.files.industry_insight.name.replace(/ /g,"_");
          industry_insight.mv(backend_path+industry_insight_name);
          industry_insight.mv(frontend_path+industry_insight_name);
        }

        partner_name = '';
        if(req.files.partner){
          let partner = req.files.partner;
          partner_name = uni+'_3_'+req.files.partner.name.replace(/ /g,"_");
          partner.mv(backend_path+partner_name);
          partner.mv(frontend_path+partner_name);
        }

        security_name = '';
        if(req.files.security){
          let security = req.files.security;
          security_name = uni+'_3_'+req.files.security.name.replace(/ /g,"_");
          security.mv(backend_path+security_name);
          security.mv(frontend_path+security_name);
        }

        revenue_management_reporting_name = '';
        if(req.files.revenue_management_reporting){
          let revenue_management_reporting = req.files.revenue_management_reporting;
          revenue_management_reporting_name = uni+'_3_'+req.files.revenue_management_reporting.name.replace(/ /g,"_");
          revenue_management_reporting.mv(backend_path+revenue_management_reporting_name);
          revenue_management_reporting.mv(frontend_path+revenue_management_reporting_name);
        }

        marketing_and_branding_name = '';
        if(req.files.marketing_and_branding){
          let marketing_and_branding = req.files.marketing_and_branding;
          marketing_and_branding_name = uni+'_3_'+req.files.marketing_and_branding.name.replace(/ /g,"_");
          marketing_and_branding.mv(backend_path+marketing_and_branding_name);
          marketing_and_branding.mv(frontend_path+marketing_and_branding_name);
        }

        sharkpads_way_name = '';
        if(req.files.sharkpads_way){
          let sharkpads_way = req.files.sharkpads_way;
          sharkpads_way_name = uni+'_3_'+req.files.sharkpads_way.name.replace(/ /g,"_");
          sharkpads_way.mv(backend_path+sharkpads_way_name);
          sharkpads_way.mv(frontend_path+sharkpads_way_name);
        }


        db.query('Select * from real_estate', function(err,result){
          if (err) {  
            res.status(200).send({ msg:err.sqlMessage });
          }else{
            console.log(result,'result');

            if(result.length > 0){

              if(req.files.uploads){
                if (result[0].header_video && fs.existsSync(backend_path+result[0].header_video)) {
                  fs.unlinkSync(backend_path+result[0].header_video,function(err){
                    console.log(err);
                  }); 
                }

                if (result[0].header_video && fs.existsSync(frontend_path+result[0].header_video)) {
                  fs.unlinkSync(frontend_path+result[0].header_video,function(err){
                    console.log(err);
                  }); 
                }
              }else{
                uploads = result[0].pic_one;
              }

              if(req.files.pic_one){
                if (result[0].pic_one && fs.existsSync(backend_path+result[0].pic_one)) {
                  fs.unlinkSync(backend_path+result[0].pic_one,function(err){
                    console.log(err);
                  }); 
                }

                if (result[0].pic_one && fs.existsSync(frontend_path+result[0].pic_one)) {
                  fs.unlinkSync(frontend_path+result[0].pic_one,function(err){
                    console.log(err);
                  }); 
                }
              }else{
                file_name = result[0].pic_one;
              }

              if(req.files.pic_two){
                if (result[0].pic_two && fs.existsSync(backend_path+result[0].pic_two)) {
                  fs.unlinkSync(backend_path+result[0].pic_two,function(err){
                    console.log(err);
                  }); 
                }

                if (result[0].pic_two && fs.existsSync(frontend_path+result[0].pic_two)) {
                  fs.unlinkSync(frontend_path+result[0].pic_two,function(err){
                    console.log(err);
                  }); 
                }
              }else{
                  file_name1 = result[0].pic_two;
              }

              if(req.files.pic_three){
                if (result[0].pic_three && fs.existsSync(backend_path+result[0].pic_three)) {
                  fs.unlinkSync(backend_path+result[0].pic_three,function(err){
                    console.log(err);
                  }); 
                }

                if (result[0].pic_three && fs.existsSync(frontend_path+result[0].pic_three)) {
                  fs.unlinkSync(frontend_path+result[0].pic_three,function(err){
                    console.log(err);
                  }); 
                }
              }else{
                  file_name2 = result[0].pic_three;
              }

              if(req.files.personal_portfolio){
                if (result[0].personal_portfolio && fs.existsSync(backend_path+result[0].personal_portfolio)) {
                  fs.unlinkSync(backend_path+result[0].personal_portfolio,function(err){
                    console.log(err);
                  }); 
                }

                if (result[0].personal_portfolio && fs.existsSync(frontend_path+result[0].personal_portfolio)) {
                  fs.unlinkSync(frontend_path+result[0].personal_portfolio,function(err){
                    console.log(err);
                  }); 
                }
              }else{
                personal_portfolio_name = result[0].personal_portfolio;
              }

              if(req.files.technology){
                if (result[0].technology && fs.existsSync(backend_path+result[0].technology)) {
                  fs.unlinkSync(backend_path+result[0].technology,function(err){
                    console.log(err);
                  }); 
                }

                if (result[0].technology && fs.existsSync(frontend_path+result[0].technology)) {
                  fs.unlinkSync(frontend_path+result[0].technology,function(err){
                    console.log(err);
                  }); 
                }
              }else{
                technology_name = result[0].technology;
              }

              if(req.files.community_relations){
                if (result[0].community_relations && fs.existsSync(backend_path+result[0].community_relations)) {
                  fs.unlinkSync(backend_path+result[0].community_relations,function(err){
                    console.log(err);
                  }); 
                }

                if (result[0].community_relations && fs.existsSync(frontend_path+result[0].community_relations)) {
                  fs.unlinkSync(frontend_path+result[0].community_relations,function(err){
                    console.log(err);
                  }); 
                }
              }else{
                community_relations_name = result[0].community_relations;
              }

              if(req.files.identification){
                if (result[0].identification && fs.existsSync(backend_path+result[0].identification)) {
                  fs.unlinkSync(backend_path+result[0].identification,function(err){
                    console.log(err);
                  }); 
                }

                if (result[0].identification && fs.existsSync(frontend_path+result[0].identification)) {
                  fs.unlinkSync(frontend_path+result[0].identification,function(err){
                    console.log(err);
                  }); 
                }
              }else{
                identification_name = result[0].identification;
              }

              if(req.files.contract_and_negotitations){
                if (result[0].contract_and_negotitations && fs.existsSync(backend_path+result[0].contract_and_negotitations)) {
                  fs.unlinkSync(backend_path+result[0].contract_and_negotitations,function(err){
                    console.log(err);
                  }); 
                }

                if (result[0].contract_and_negotitations && fs.existsSync(frontend_path+result[0].contract_and_negotitations)) {
                  fs.unlinkSync(frontend_path+result[0].contract_and_negotitations,function(err){
                    console.log(err);
                  }); 
                }
              }else{
                contract_and_negotitations_name = result[0].contract_and_negotitations;
              }

              if(req.files.industry_insight){
                if (result[0].industry_insight && fs.existsSync(backend_path+result[0].industry_insight)) {
                  fs.unlinkSync(backend_path+result[0].industry_insight,function(err){
                    console.log(err);
                  }); 
                }

                if (result[0].industry_insight && fs.existsSync(frontend_path+result[0].industry_insight)) {
                  fs.unlinkSync(frontend_path+result[0].industry_insight,function(err){
                    console.log(err);
                  }); 
                }
              }else{
                industry_insight_name = result[0].industry_insight;
              }

              if(req.files.partner){
                if (result[0].partner && fs.existsSync(backend_path+result[0].partner)) {
                  fs.unlinkSync(backend_path+result[0].partner,function(err){
                    console.log(err);
                  }); 
                }

                if (result[0].partner && fs.existsSync(frontend_path+result[0].partner)) {
                  fs.unlinkSync(frontend_path+result[0].partner,function(err){
                    console.log(err);
                  }); 
                }
              }else{
                partner_name = result[0].partner;
              }

              if(req.files.security){
                if (result[0].security && fs.existsSync(backend_path+result[0].security)) {
                  fs.unlinkSync(backend_path+result[0].security,function(err){
                    console.log(err);
                  }); 
                }

                if (result[0].security && fs.existsSync(frontend_path+result[0].security)) {
                  fs.unlinkSync(frontend_path+result[0].security,function(err){
                    console.log(err);
                  }); 
                }
              }else{
                security_name = result[0].security;
              }

              if(req.files.revenue_management_reporting){
                if (result[0].revenue_management_reporting && fs.existsSync(backend_path+result[0].revenue_management_reporting)) {
                  fs.unlinkSync(backend_path+result[0].revenue_management_reporting,function(err){
                    console.log(err);
                  }); 
                }

                if (result[0].revenue_management_reporting && fs.existsSync(frontend_path+result[0].revenue_management_reporting)) {
                  fs.unlinkSync(frontend_path+result[0].revenue_management_reporting,function(err){
                    console.log(err);
                  }); 
                }
              }else{
                revenue_management_reporting_name = result[0].revenue_management_reporting;
              }

              if(req.files.marketing_and_branding){
                if (result[0].marketing_and_branding && fs.existsSync(backend_path+result[0].marketing_and_branding)) {
                  fs.unlinkSync(backend_path+result[0].marketing_and_branding,function(err){
                    console.log(err);
                  }); 
                }

                if (result[0].marketing_and_branding && fs.existsSync(frontend_path+result[0].marketing_and_branding)) {
                  fs.unlinkSync(frontend_path+result[0].marketing_and_branding,function(err){
                    console.log(err);
                  }); 
                }
              }else{
                marketing_and_branding_name = result[0].marketing_and_branding;
              }

              if(req.files.sharkpads_way){
                if (result[0].sharkpads_way && fs.existsSync(backend_path+result[0].sharkpads_way)) {
                  fs.unlinkSync(backend_path+result[0].sharkpads_way,function(err){
                    console.log(err);
                  }); 
                }

                if (result[0].sharkpads_way && fs.existsSync(frontend_path+result[0].sharkpads_way)) {
                  fs.unlinkSync(frontend_path+result[0].sharkpads_way,function(err){
                    console.log(err);
                  }); 
                }
              }else{
                sharkpads_way_name = result[0].sharkpads_way;
              }

              results=JSON.parse(JSON.stringify(result));
              
              var sql = "UPDATE `real_estate` set page_title=?,marketing_and_branding_text=?,revenue_management_reporting_text=?,security_text=?,partner_text=?,industry_insight_text=?,contract_and_negotitations_text=?,identification_text=?,community_relations_text=?,technology_text=?,personal_portfolio_text=?,sharkpads_way_text=?,personal_portfolio=?,technology=?,community_relations=?,identification=?,contract_and_negotitations=?,industry_insight=?,partner=?,security=?,sharkpads_way=?,revenue_management_reporting=?,marketing_and_branding=?,header_video =?,pic_one =?,pic_two =?,pic_three =?,content_one =?  WHERE id = ?";
              db.query(sql,[page_title,marketing_and_branding_text,revenue_management_reporting_text,security_text,partner_text,industry_insight_text,contract_and_negotitations_text,identification_text,community_relations_text,technology_text,personal_portfolio_text,sharkpads_way_text,personal_portfolio_name,technology_name,community_relations_name,identification_name,contract_and_negotitations_name,industry_insight_name,partner_name,security_name,sharkpads_way_name,revenue_management_reporting_name,marketing_and_branding_name,uploads,file_name,file_name1,file_name2,content_one,results[0].id], (err, result) => {
               if (err) {
                 res.status(200).send({ status: false,msg:err.sqlMessage });
               }else{
                 res.status(200).send({ msg: 'Page updated successfully'}); 
               }
             });
            }else{
              let query = "INSERT INTO `real_estate` (page_id,header_video,pic_one,pic_two,pic_three,content_one,personal_portfolio,technology,community_relations,identification,contract_and_negotitations,industry_insight,partner,security,sharkpads_way,revenue_management_reporting,marketing_and_branding,sharkpads_way_text,marketing_and_branding_text,revenue_management_reporting_text,security_text,partner_text,industry_insight_text,contract_and_negotitations_text,identification_text,community_relations_text,technology_text,personal_portfolio_text,page_title) VALUES ('" + req.body.page_id + "','" +
                  uploads + "','" + file_name + "','" + file_name1 + "','" + file_name2 + "','" + personal_portfolio_name + "','" + technology_name + "','" + community_relations_name + "','" + identification_name + "','" + contract_and_negotitations_name + "','" + industry_insight_name + "','" + partner_name + "','" + security_name + "','" + sharkpads_way_name + "','" + revenue_management_reporting_name + "','" + marketing_and_branding + "','" + sharkpads_way_text + "','" + marketing_and_branding_text + "','" + revenue_management_reporting_text + "','" + security_text +"','" + partner_text + "','" + industry_insight_text  +"','" + contract_and_negotitations_text + "','" + identification_text + "','" + community_relations_text + "','" + technology_text + "','" + personal_portfolio_text + "','" + page_title + "')";
                db.query(query, (err, result) => {
                if (err) {
                  res.status(200).send({ msg:err.sqlMessage });
                }else{
                  res.status(200).send({ msg: 'Page updated successfully'}); 
                }
              });
            }
          }
        });
        }else{
          db.query('Select * from real_estate', function(err,results){
          if (err) {  
              res.status(200).send({ msg:err.sqlMessage });
          }else{
              
              if(results.length > 0){
                var sql = "UPDATE `real_estate` set page_title=?,marketing_and_branding_text=?,revenue_management_reporting_text=?,security_text=?,partner_text=?,industry_insight_text=?,contract_and_negotitations_text=?,identification_text=?,community_relations_text=?,technology_text=?,personal_portfolio_text=?,sharkpads_way_text=?,content_one =? WHERE id = ?";
                db.query(sql,[page_title,marketing_and_branding_text,revenue_management_reporting_text,security_text,partner_text,industry_insight_text,contract_and_negotitations_text,identification_text,community_relations_text,technology_text,personal_portfolio_text,sharkpads_way_text,content_one,results[0].id], (err, result) => {
                 if (err) {
                   res.status(200).send({ status: false,msg:err.sqlMessage });
                 }else{
                   res.status(200).send({ msg: 'Page updated successfully'}); 
                 }
               });
              }else{
                let query = "INSERT INTO `real_estate` (page_id,content_one,sharkpads_way_text,marketing_and_branding_text,revenue_management_reporting_text,security_text,partner_text,industry_insight_text,contract_and_negotitations_text,identification_text,community_relations_text,technology_text,personal_portfolio_text,page_title) VALUES ('" + req.body.page_id + "','" + content_one + "','" + sharkpads_way_text + "','" + marketing_and_branding_text + "','" + revenue_management_reporting_text + "','" + security_text +"','" + partner_text + "','" + industry_insight_text  +"','" + contract_and_negotitations_text + "','" + identification_text + "','" + community_relations_text + "','" + technology_text + "','" + personal_portfolio_text + "','"+ page_title + "')";
                db.query(query, (err, result) => {
                if (err) {
                  res.status(200).send({ msg:err.sqlMessage });
                }else{
                  res.status(200).send({ msg: 'Page updated successfully'}); 
                }
              });
              }
            } 
          });  
      } 
  });

  app.post('/api_slide',  function (req, res) {
      var caption = req.body.caption;
      var link = req.body.link;

      if(req.files){
        console.log(req.files,'files');
        let uni = new Date().getTime();

        uploads = '';
        if(req.files.uploads){
          let sampleFile_uploads = req.files.uploads;
          uploads = uni+'_'+req.files.uploads.name.replace(/ /g,"_");
          sampleFile_uploads.mv(backend_path+uploads);
          sampleFile_uploads.mv(frontend_path+uploads);
        }
      }else{
        res.status(200).send({ msg: 'No image selected',status:false});
      }
        
      let query = "INSERT INTO `sliders` (image,caption) VALUES ('" + uploads + "','" + caption + "')";
        db.query(query, (err, result) => {
        if (err) {
          res.status(200).send({ msg:err.sqlMessage,status:false });
        }else{
          res.status(200).send({ msg: 'Page updated successfully',status:true}); 
        }
      });
  });

  app.post('/api_slide_update',  function (req, res) {
      var caption = req.body.caption;
      var link = req.body.link;

      var uploads = req.body.current_image;
      if(req.files){
        let uni = new Date().getTime();

        
        if(req.files.uploads){
          let sampleFile_uploads = req.files.uploads;
          uploads = uni+'_'+req.files.uploads.name.replace(/ /g,"_");
          sampleFile_uploads.mv(backend_path+uploads);
          sampleFile_uploads.mv(frontend_path+uploads);
        }

        if(req.files.uploads){
          if (fs.existsSync(backend_path+req.body.current_image)) {
            fs.unlinkSync(backend_path+req.body.current_image,function(err){
              console.log(err);
            }); 
          }

          if (fs.existsSync(frontend_path+req.body.current_image)) {
            fs.unlinkSync(frontend_path+req.body.current_image,function(err){
              console.log(err);
            }); 
          }
        }
      }
        
      var sql = "UPDATE `sliders` set caption =?,image =?,link =? WHERE id = ?";
      db.query(sql,[caption,uploads,link,results[0].id], (err, result) => {
       if (err) {
         res.status(200).send({ status: false,msg:err.sqlMessage });
       }else{
         res.status(200).send({ status: true,msg: 'Slide updated successfully'}); 
       }
     });
  });

  app.post('/get_homepage_video', function (req, res) {
    db.query('Select * from hompeage_content', function(err,result){
      if (err) {  
        res.status(200).send({ page: result,error:err });
      }else{
        results=JSON.parse(JSON.stringify(result));
        res.status(200).send({ homepage_video: results[0] });
      }
    }); 
  });

  app.delete('/slide/:id', function (req, res) {
    db.query('delete from sliders where id='+req.params.id, function(err,result){
      if (err) {  
        res.status(200).send({ status:false,msg:err.sqlMessage });
      }else{
        res.status(200).send({ status:true,msg: 'Slider Deleted successfully.' });
      }
    }); 
  });

  app.get('/get_slide_info/:id', function (req, res) {
    //console.log(req,'req');

    db.query('Select * from sliders where id='+req.params.id, function(err,result){
      if (err) {  
        res.status(200).send({ page: result,error:err });
      }else{
        results=JSON.parse(JSON.stringify(result));
        res.status(200).send({ homepage_video: results[0] });
      }
    }); 
  });

  app.post('/get_rentals', function (req, res) {
    db.query('Select * from rentals limit 1', function(err,result){
      if (err) {  
        res.status(200).send({ page: result,error:err });
      }else{
        results=JSON.parse(JSON.stringify(result));
        res.status(200).send({ traveldesign: results[0] });
      }
    }); 
  });

  app.post('/get_traveldesign', function (req, res) {
    db.query('Select * from traveldesign limit 1', function(err,result){
      if (err) {  
        res.status(200).send({ page: result,error:err });
      }else{
        results=JSON.parse(JSON.stringify(result));
        res.status(200).send({ traveldesign: results[0] });
      }
    }); 
  });

  app.post('/get_settings', function (req, res) {
    db.query('Select * from settings limit 1', function(err,result){
      if (err) {  
        res.status(200).send({ page: result,error:err });
      }else{
        results=JSON.parse(JSON.stringify(result));
        res.status(200).send({ traveldesign: results[0] });
      }
    }); 
  });

  app.post('/get_lifestyle', function (req, res) {
    db.query('Select * from life_style limit 1', function(err,result){
      if (err) {  
        res.status(200).send({ page: result,error:err });
      }else{
        results=JSON.parse(JSON.stringify(result));
        res.status(200).send({ traveldesign: results[0] });
      }
    }); 
  });

  app.post('/get_real_estate', function (req, res) {
    db.query('Select * from real_estate limit 1', function(err,result){
      if (err) {  
        res.status(200).send({ page: result,error:err });
      }else{
        results=JSON.parse(JSON.stringify(result));
        res.status(200).send({ traveldesign: results[0] });
      }
    }); 
  });


  app.get('/get_pages', function (req, res) {
    db.query('Select * from page order by display_order asc', function(err,result){
      if (err) {  
        res.status(200).send({ pages: result,error:err });
      }else{
        res.status(200).send({ pages: result,error:'' });
      }
    }); 
  });

  app.get('/get_slider', function (req, res) {
    db.query('Select * from sliders order by id desc', function(err,result){
      if (err) {  
        res.status(200).send({ pages: result,error:err });
      }else{
        res.status(200).send({ pages: result,error:'' });
      }
    }); 
  });

  app.get('/get_page/:id', function (req, res){
      let query = "Select * from page where id = ('" + req.params.id + "')";
      
      db.query(query, function(err,result){
        res.status(200).send({ page: result,error:err });
      }); 
     
  }); 

  app.post('/save_page', function (req, res) {
     console.log(req.body)

     let query = "INSERT INTO `page`  (name,content,status) VALUES ('" + req.body.page_name + "','" +
      req.body.page_content + "','" + req.body.page_status + "')";
      db.query(query, (err, result) => {
        
        console.log(err,'err');

      if (err) {
        res.status(200).send({ status: false,msg:err });
      }else{
        res.status(200).send({ status: true });
      }
    }); 
  });

  app.post('/update_status', function (req, res) {
    var sql = "UPDATE `page` set status =?  WHERE id = ?";

    db.query(sql,[req.body.status,req.body.id], (err, result) => {
       
       console.log(err,'err');

     if (err) {
       res.status(200).send({ status: false,msg:err });
     }else{
       res.status(200).send({ status: true });
     }
   }); 
 });
  
  app.post('/update_page', function (req, res) {
    var sql = "UPDATE `page` set name =? , content =?, status =?, url =?  WHERE id = ?";
    let status = req.body.page_status == 'Active' ? 1 : 0;

    db.query(sql,[req.body.page_name,req.body.page_content,status,req.body.page_link,req.body.id], (err, result) => {
       
       console.log(err,'err');

     if (err) {
       res.status(200).send({ status: false,msg:err });
     }else{
       res.status(200).send({ status: true });
     }
   }); 
 });
};