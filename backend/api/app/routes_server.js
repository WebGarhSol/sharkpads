module.exports = function (app, passport, fs) {

  app.post('/api_homepage',  function (req, res) {
      var file_name = req.query.uploads;

      if(req.files){
        let sampleFile = req.files.uploads;
        let uni = new Date().getTime();
        file_name = uni+'_'+req.files.uploads.name.replace(/ /g,"_");
        sampleFile.mv('./../backend/assets/page/'+file_name);
        sampleFile.mv('./../frontend/assets/page/'+file_name);

        db.query('Select * from hompeage_content', function(err,result){
          if (err) {  
            res.status(200).send({ msg:err.sqlMessage });
          }else{
            console.log(result,'result');

            if(result.length > 0){

              if (result[0].homepage_video && fs.existsSync('./../backend/'+result[0].homepage_video)) {
                fs.unlinkSync('./../backend/'+result[0].homepage_video,function(err){
                  console.log(err);
                }); 
              }

              if (result[0].homepage_video && fs.existsSync('./../frontend/'+result[0].homepage_video)) {
                fs.unlinkSync('./../frontend/'+result[0].homepage_video,function(err){
                  console.log(err);
                }); 
              }

              results=JSON.parse(JSON.stringify(result));
              
              var sql = "UPDATE `hompeage_content` set homepage_video =?  WHERE id = ?";
              db.query(sql,[file_name,results[0].id], (err, result) => {
                 
                 console.log(err,'err');

               if (err) {
                 res.status(200).send({ status: false,msg:err.sqlMessage });
               }else{
                 res.status(200).send({ msg: 'file uploaded successfully',name:file_name}); 
               }
             });
            }else{
              let query = "INSERT INTO `hompeage_content`  (page_id,homepage_video) VALUES ('" + req.body.page_id + "','" +
                  file_name + "')";
                db.query(query, (err, result) => {
                if (err) {
                  res.status(200).send({ msg:err.sqlMessage });
                }else{
                  res.status(200).send({ msg: 'file uploaded successfully',name:file_name}); 
                }
              });
            }
          }
        });
        }else{
          res.status(200).send({ msg: 'no file uploaded'});
      } 
  });

  app.post('/api_travel_design',  function (req, res) {
      var content_one = req.body.content_one;
      var content_two = req.body.content_two;

      if(req.files){
        console.log(req.files.pic_one,'files');
        let uni = new Date().getTime();
        
        file_name = '';
        if(req.files.uploads){
          let sampleFile = req.files.uploads;
          file_name = uni+'_1_'+req.files.uploads.name.replace(/ /g,"_");
          sampleFile.mv('./../backend/assets/page/'+file_name);
          sampleFile.mv('./../frontend/assets/page/'+file_name);
        }

        file_name1 = '';
        if(req.files.pic_one){
          let sampleFile1 = req.files.pic_one;
          file_name1 = uni+'_2_'+req.files.pic_one.name.replace(/ /g,"_");
          sampleFile1.mv('./../backend/assets/page/'+file_name1); 
          sampleFile1.mv('./../frontend/assets/page/'+file_name1); 
        }

        file_name2 = '';
        if(req.files.pic_two){
          let sampleFile2 = req.files.pic_two;
          file_name2 = uni+'_3_'+req.files.pic_two.name.replace(/ /g,"_");
          sampleFile2.mv('./../backend/assets/page/'+file_name2);
          sampleFile2.mv('./../frontend/assets/page/'+file_name2);
        }

        db.query('Select * from traveldesign', function(err,result){
          if (err) {  
            res.status(200).send({ msg:err.sqlMessage });
          }else{
            console.log(result,'result');

            if(result.length > 0){

              if(req.files.uploads){
                if (result[0].header_video && fs.existsSync('./../backend/'+result[0].header_video)) {
                  fs.unlinkSync('./../backend/'+result[0].header_video,function(err){
                    console.log(err);
                  }); 
                }

                if (result[0].header_video && fs.existsSync('./../frontend/'+result[0].header_video)) {
                  fs.unlinkSync('./../frontend/'+result[0].header_video,function(err){
                    console.log(err);
                  }); 
                }
              }else{
                file_name = result[0].header_video;
              }

              if(req.files.pic_one){
                if (result[0].pic_one && fs.existsSync('./../backend/'+result[0].pic_one)) {
                  fs.unlinkSync('./../backend/'+result[0].pic_one,function(err){
                    console.log(err);
                  }); 
                }

                if (result[0].pic_one && fs.existsSync('./../frontend/'+result[0].pic_one)) {
                  fs.unlinkSync('./../frontend/'+result[0].pic_one,function(err){
                    console.log(err);
                  }); 
                }
              }else{
                  file_name1 = result[0].pic_one;
              }

              if(req.files.pic_two){
                if (result[0].pic_two && fs.existsSync('./../backend/'+result[0].pic_two)) {
                  fs.unlinkSync('./../backend/'+result[0].pic_two,function(err){
                    console.log(err);
                  }); 
                }

                if (result[0].pic_two && fs.existsSync('./../frontend/'+result[0].pic_two)) {
                  fs.unlinkSync('./../frontend/'+result[0].pic_two,function(err){
                    console.log(err);
                  }); 
                }
              }else{
                  file_name2 = result[0].pic_two;
              }

              results=JSON.parse(JSON.stringify(result));
              
              var sql = "UPDATE `traveldesign` set header_video =?,pic_one =?,pic_two =?,content_one =?,content_two =?  WHERE id = ?";
              db.query(sql,[file_name,file_name1,file_name2,content_one,content_two,results[0].id], (err, result) => {
               if (err) {
                 res.status(200).send({ status: false,msg:err.sqlMessage });
               }else{
                 res.status(200).send({ msg: 'Page updated successfully'}); 
               }
             });
            }else{
              let query = "INSERT INTO `traveldesign` (page_id,header_video,pic_one,pic_two,content_one,content_two) VALUES ('" + req.body.page_id + "','" +
                  file_name + "','" + file_name1 + "','" + file_name2 + "','" + content_one + "','" + content_two + "')";
                db.query(query, (err, result) => {
                if (err) {
                  res.status(200).send({ msg:err.sqlMessage });
                }else{
                  res.status(200).send({ msg: 'Page updated successfully'}); 
                }
              });
            }
          }
        });
        }else{
          db.query('Select * from traveldesign', function(err,result){
          if (err) {  
              res.status(200).send({ msg:err.sqlMessage });
          }else{
              
              if(result.length > 0){
                var sql = "UPDATE `traveldesign` set content_one =?,content_two =?  WHERE id = ?";
                db.query(sql,[content_one,content_two,results[0].id], (err, result) => {
                 if (err) {
                   res.status(200).send({ status: false,msg:err.sqlMessage });
                 }else{
                   res.status(200).send({ msg: 'Page updated successfully'}); 
                 }
               });
              }else{
                let query = "INSERT INTO `traveldesign` (page_id,content_one,content_two) VALUES ('" + req.body.page_id + "','" + content_one + "','" + content_two + "')";
                db.query(query, (err, result) => {
                if (err) {
                  res.status(200).send({ msg:err.sqlMessage });
                }else{
                  res.status(200).send({ msg: 'Page updated successfully'}); 
                }
              });
              }
            } 
          });  
      } 
  });

  app.post('/api_life_style',  function (req, res) {
      var content_one = req.body.content_one;
      var content_two = req.body.content_two;
      var content_three = req.body.content_three;
      var content_four = req.body.content_four;

      if(req.files){
        console.log(req.files.pic_one,'files');

        let uni = new Date().getTime();
        
        file_name = '';
        if(req.files.uploads){
          let sampleFile = req.files.uploads;
          file_name = uni+'_1_'+req.files.uploads.name.replace(/ /g,"_");
          sampleFile.mv('./../backend/assets/page/'+file_name);
          sampleFile.mv('./../frontend/assets/page/'+file_name);
        }

        file_name1 = '';
        if(req.files.pic_one){
          let sampleFile1 = req.files.pic_one;
          file_name1 = uni+'_2_'+req.files.pic_one.name.replace(/ /g,"_");
          sampleFile1.mv('./../backend/assets/page/'+file_name1); 
          sampleFile1.mv('./../frontend/assets/page/'+file_name1); 
        }

        file_name2 = '';
        if(req.files.pic_two){
          let sampleFile2 = req.files.pic_two;
          file_name2 = uni+'_3_'+req.files.pic_two.name.replace(/ /g,"_");
          sampleFile2.mv('./../backend/assets/page/'+file_name2);
          sampleFile2.mv('./../frontend/assets/page/'+file_name2);
        }

        file_name3 = '';
        if(req.files.pic_three){
          let sampleFile3 = req.files.pic_three;
          
          console.log(sampleFile3,'sampleFile3')  

          file_name3 = uni+'_4_'+req.files.pic_three.name.replace(/ /g,"_");
          sampleFile3.mv('./../backend/assets/page/'+file_name3);
          sampleFile3.mv('./../frontend/assets/page/'+file_name3);
        }

        db.query('Select * from life_style', function(err,result){
          if (err) {  
            res.status(200).send({ msg:err.sqlMessage });
          }else{
            console.log(result,'result');

            if(result.length > 0){

              if(req.files.uploads){
                if (result[0].header_video && fs.existsSync('./../backend/'+result[0].header_video)) {
                  fs.unlinkSync('./../backend/'+result[0].header_video,function(err){
                    console.log(err);
                  }); 
                }

                if (result[0].header_video && fs.existsSync('./../frontend/'+result[0].header_video)) {
                  fs.unlinkSync('./../frontend/'+result[0].header_video,function(err){
                    console.log(err);
                  }); 
                }
              }else{
                file_name = result[0].header_video;
              }

              if(req.files.pic_one){
                if (result[0].pic_one && fs.existsSync('./../backend/'+result[0].pic_one)) {
                  fs.unlinkSync('./../backend/'+result[0].pic_one,function(err){
                    console.log(err);
                  }); 
                }

                if (result[0].pic_one && fs.existsSync('./../frontend/'+result[0].pic_one)) {
                  fs.unlinkSync('./../frontend/'+result[0].pic_one,function(err){
                    console.log(err);
                  }); 
                }
              }else{
                  file_name1 = result[0].pic_one;
              }

              if(req.files.pic_two){
                if (result[0].pic_two && fs.existsSync('./../backend/'+result[0].pic_two)) {
                  fs.unlinkSync('./../backend/'+result[0].pic_two,function(err){
                    console.log(err);
                  }); 
                }

                if (result[0].pic_two && fs.existsSync('./../frontend/'+result[0].pic_two)) {
                  fs.unlinkSync('./../frontend/'+result[0].pic_two,function(err){
                    console.log(err);
                  }); 
                }
              }else{
                  file_name2 = result[0].pic_two;
              }

              if(req.files.pic_three){
                if (result[0].pic_three && fs.existsSync('./../backend/'+result[0].pic_three)) {
                  fs.unlinkSync('./../backend/'+result[0].pic_three,function(err){
                    console.log(err);
                  }); 
                }

                if (result[0].pic_three && fs.existsSync('./../frontend/'+result[0].pic_three)) {
                  fs.unlinkSync('./../frontend/'+result[0].pic_three,function(err){
                    console.log(err);
                  }); 
                }
              }else{
                  file_name3 = result[0].pic_three;
              }

              results=JSON.parse(JSON.stringify(result));
              
              var sql = "UPDATE `life_style` set header_video =?,pic_one =?,pic_two =?,pic_three =?,content_one =?,content_two =?,content_three =?,content_four =? WHERE id = ?";
              db.query(sql,[file_name,file_name1,file_name2,file_name3,content_one,content_two,content_three,content_four,results[0].id], (err, result) => {
                console.log('err',err)
               if (err) {
                 res.status(200).send({ status: false,msg:err.sqlMessage });
               }else{
                 res.status(200).send({ msg: 'Page updated successfully'}); 
               }
             });
            }else{
              let query = "INSERT INTO `life_style` (page_id,header_video,pic_one,pic_two,pic_three,content_one,content_two,content_three,content_four) VALUES ('" + req.body.page_id + "','" +
                  file_name + "','" + file_name1 + "','" + file_name2 + "','" + file_name3 + "','" + content_one + "','" + content_two + "','" + content_three + "','" + content_four + "')";
                db.query(query, (err, result) => {
                if (err) {
                  res.status(200).send({ msg:err.sqlMessage });
                }else{
                  res.status(200).send({ msg: 'Page updated successfully'}); 
                }
              });
            }
          }
        });
        }else{
          db.query('Select * from life_style', function(err,result){
          if (err) {  
              res.status(200).send({ msg:err.sqlMessage });
          }else{
              
              if(result.length > 0){
                var sql = "UPDATE `life_style` set content_one =?,content_two =?,content_three =?,content_four =?  WHERE id = ?";
                db.query(sql,[content_one,content_two,content_three,content_four,results[0].id], (err, result) => {
                 if (err) {
                   res.status(200).send({ status: false,msg:err.sqlMessage });
                 }else{
                   res.status(200).send({ msg: 'Page updated successfully'}); 
                 }
               });
              }else{
                let query = "INSERT INTO `life_style` (page_id,content_one,content_two,content_three,content_four) VALUES ('" + req.body.page_id + "','" + content_one + "','" + content_two + "','" + content_three + "','" + content_four + "')";
                db.query(query, (err, result) => {
                if (err) {
                  res.status(200).send({ msg:err.sqlMessage });
                }else{
                  res.status(200).send({ msg: 'Page updated successfully'}); 
                }
              });
              }
            } 
          });  
      } 
  });

  app.post('/api_rentals',  function (req, res) {
      var content_one = req.body.content_one;
      var content_two = req.body.content_two;

      if(req.files){
        console.log(req.files.pic_one,'files');

        let uni = new Date().getTime();
        
        file_name = '';
        if(req.files.uploads){
          let sampleFile = req.files.uploads;
          file_name = uni+'_1_'+req.files.uploads.name.replace(/ /g,"_");
          sampleFile.mv('./../backend/assets/page/'+file_name);
          sampleFile.mv('./../frontend/assets/page/'+file_name);
        }

        file_name1 = '';
        if(req.files.pic_one){
          let sampleFile1 = req.files.pic_one;
          file_name1 = uni+'_2_'+req.files.pic_one.name.replace(/ /g,"_");
          sampleFile1.mv('./../backend/assets/page/'+file_name1); 
          sampleFile1.mv('./../frontend/assets/page/'+file_name1); 
        }

        file_name2 = '';
        if(req.files.pic_two){
          let sampleFile2 = req.files.pic_two;
          file_name2 = uni+'_3_'+req.files.pic_two.name.replace(/ /g,"_");
          sampleFile2.mv('./../backend/assets/page/'+file_name2);
          sampleFile2.mv('./../frontend/assets/page/'+file_name2);
        }

        db.query('Select * from rentals', function(err,result){
          if (err) {  
            res.status(200).send({ msg:err.sqlMessage });
          }else{
            console.log(result,'result');

            if(result.length > 0){

              if(req.files.uploads){
                if (result[0].header_video && fs.existsSync('./../backend/'+result[0].header_video)) {
                  fs.unlinkSync('./../backend/'+result[0].header_video,function(err){
                    console.log(err);
                  }); 
                }

                if (result[0].header_video && fs.existsSync('./../frontend/'+result[0].header_video)) {
                  fs.unlinkSync('./../frontend/'+result[0].header_video,function(err){
                    console.log(err);
                  }); 
                }
              }else{
                file_name = result[0].header_video;
              }

              if(req.files.pic_one){
                if (result[0].pic_one && fs.existsSync('./../backend/'+result[0].pic_one)) {
                  fs.unlinkSync('./../backend/'+result[0].pic_one,function(err){
                    console.log(err);
                  }); 
                }

                if (result[0].pic_one && fs.existsSync('./../frontend/'+result[0].pic_one)) {
                  fs.unlinkSync('./../frontend/'+result[0].pic_one,function(err){
                    console.log(err);
                  }); 
                }
              }else{
                  file_name1 = result[0].pic_one;
              }

              if(req.files.pic_two){
                if (result[0].pic_two && fs.existsSync('./../backend/'+result[0].pic_two)) {
                  fs.unlinkSync('./../backend/'+result[0].pic_two,function(err){
                    console.log(err);
                  }); 
                }

                if (result[0].pic_two && fs.existsSync('./../frontend/'+result[0].pic_two)) {
                  fs.unlinkSync('./../frontend/'+result[0].pic_two,function(err){
                    console.log(err);
                  }); 
                }
              }else{
                  file_name2 = result[0].pic_two;
              }

              

              results=JSON.parse(JSON.stringify(result));
              
              var sql = "UPDATE `rentals` set header_video =?,pic_one =?,pic_two =?,content_one =?,content_two =? WHERE id = ?";
              db.query(sql,[file_name,file_name1,file_name2,content_one,content_two,results[0].id], (err, result) => {
                console.log('err',err)
               if (err) {
                 res.status(200).send({ status: false,msg:err.sqlMessage });
               }else{
                 res.status(200).send({ msg: 'Page updated successfully'}); 
               }
             });
            }else{
              let query = "INSERT INTO `rentals` (page_id,header_video,pic_one,pic_two,content_one,content_two) VALUES ('" + req.body.page_id + "','" +
                  file_name + "','" + file_name1 + "','" + file_name2 + "','" + content_one + "','" + content_two + "')";
                db.query(query, (err, result) => {
                if (err) {
                  res.status(200).send({ msg:err.sqlMessage });
                }else{
                  res.status(200).send({ msg: 'Page updated successfully'}); 
                }
              });
            }
          }
        });
        }else{
          db.query('Select * from rentals', function(err,result){
          if (err) {  
              res.status(200).send({ msg:err.sqlMessage });
          }else{
              
              if(result.length > 0){
                var sql = "UPDATE `rentals` set content_one =?,content_two =?  WHERE id = ?";
                db.query(sql,[content_one,content_two,results[0].id], (err, result) => {
                 if (err) {
                   res.status(200).send({ status: false,msg:err.sqlMessage });
                 }else{
                   res.status(200).send({ msg: 'Page updated successfully'}); 
                 }
               });
              }else{
                let query = "INSERT INTO `rentals` (page_id,content_one,content_two) VALUES ('" + req.body.page_id + "','" + content_one + "','" + content_two + "')";
                db.query(query, (err, result) => {
                if (err) {
                  res.status(200).send({ msg:err.sqlMessage });
                }else{
                  res.status(200).send({ msg: 'Page updated successfully'}); 
                }
              });
              }
            } 
          });  
      } 
  });

  app.post('/api_settings',  function (req, res) {
      var facebook = req.body.facebook;
      var twitter = req.body.twitter;
      var instgram = req.body.instgram;
      var linkedin = req.body.linkedin;

      var phone = req.body.phone;
      var email = req.body.email;
      var address = req.body.address;
      var common_content = req.body.common_content;

      var map = req.body.map;
      var aboutus = req.body.aboutus;

      if(req.files){
        let uni = new Date().getTime();

        file_name = '';
        if(req.files.uploads){
          let sampleFile = req.files.uploads;
          file_name = uni+'_1_'+req.files.uploads.name.replace(/ /g,"_");
          sampleFile.mv('./../backend/assets/page/'+file_name);
          sampleFile.mv('./../frontend/assets/page/'+file_name);
        }
      }  

      db.query('Select * from settings limit 1', function(err,result){
          if (err) {  
            res.status(200).send({ msg:err.sqlMessage });
          }else{
            if(result.length > 0){

                if(req.files){
                  if (result[0].pic_gallery_image && fs.existsSync('./../backend/'+result[0].pic_gallery_image)) {
                    fs.unlinkSync('./../backend/'+result[0].pic_gallery_image,function(err){
                      console.log(err);
                    }); 
                  }

                  if (result[0].pic_gallery_image && fs.existsSync('./../frontend/'+result[0].pic_gallery_image)) {
                    fs.unlinkSync('./../frontend/'+result[0].pic_gallery_image,function(err){
                      console.log(err);
                    }); 
                  }
                }else{
                  file_name = result[0].pic_gallery_image;
                }

                var sql = "UPDATE `settings` set facebook =?,twitter =?,instgram =?,linkedin =?,phone =?,email =?,address =?,common_content =?,map =?,aboutus =?,pic_gallery_image=? WHERE id = ?";
                db.query(sql,[facebook,twitter,instgram,linkedin,phone,email,address,common_content,map,aboutus,file_name,result[0].id], (err, result) => {
                 if (err) {
                   res.status(200).send({ status: false,msg:err.sqlMessage });
                 }else{
                   res.status(200).send({ msg: 'Settings updated successfully'}); 
                 }
               });
              }else{
                let query = "INSERT INTO `settings` (facebook,twitter,instgram,linkedin,phone,email,address,common_content,map,aboutus,file_name,pic_gallery_image) VALUES ('" + facebook + "','" + twitter + "','" + instgram + "','" + linkedin + "','" + phone + "','" + email + "','" + address + "','" + common_content + "','" + map + "','" + aboutus + "','" + file_name + "')";
                db.query(query, (err, result) => {
                if (err) {
                  res.status(200).send({ msg:err.sqlMessage });
                }else{
                  res.status(200).send({ msg: 'Settings updated successfully'}); 
                }
              });
            }
          }
      }); 
  });    

  app.post('/api_real_estate',  function (req, res) {
      var content_one = req.body.content_one;

      if(req.files){
        console.log(req.files,'files');
        let uni = new Date().getTime();

        uploads = '';
        if(req.files.uploads){
          let sampleFile_uploads = req.files.uploads;
          uploads = uni+'_12_'+req.files.uploads.name.replace(/ /g,"_");
          sampleFile_uploads.mv('./../backend/assets/page/'+uploads);
          sampleFile_uploads.mv('./../frontend/assets/page/'+uploads);
        }
        
        file_name = '';
        if(req.files.pic_one){
          let sampleFile = req.files.pic_one;
          file_name = uni+'_1_'+req.files.pic_one.name.replace(/ /g,"_");
          sampleFile.mv('./../backend/assets/page/'+file_name);
          sampleFile.mv('./../frontend/assets/page/'+file_name);
        }

        file_name1 = '';
        if(req.files.pic_two){
          let sampleFile1 = req.files.pic_two;
          file_name1 = uni+'_2_'+req.files.pic_two.name.replace(/ /g,"_");
          sampleFile1.mv('./../backend/assets/page/'+file_name1); 
          sampleFile1.mv('./../frontend/assets/page/'+file_name1); 
        }

        file_name2 = '';
        if(req.files.pic_three){
          let sampleFile2 = req.files.pic_three;
          file_name2 = uni+'_3_'+req.files.pic_three.name.replace(/ /g,"_");
          sampleFile2.mv('./../backend/assets/page/'+file_name2);
          sampleFile2.mv('./../frontend/assets/page/'+file_name2);
        }

        db.query('Select * from real_estate', function(err,result){
          if (err) {  
            res.status(200).send({ msg:err.sqlMessage });
          }else{
            console.log(result,'result');

            if(result.length > 0){

              if(req.files.uploads){
                if (result[0].header_video && fs.existsSync('./../backend/'+result[0].pic_one)) {
                  fs.unlinkSync('./../backend/'+result[0].header_video,function(err){
                    console.log(err);
                  }); 
                }

                if (result[0].header_video && fs.existsSync('./../frontend/'+result[0].pic_one)) {
                  fs.unlinkSync('./../frontend/'+result[0].header_video,function(err){
                    console.log(err);
                  }); 
                }
              }else{
                uploads = result[0].pic_one;
              }

              if(req.files.pic_one){
                if (result[0].pic_one && fs.existsSync('./../backend/'+result[0].pic_one)) {
                  fs.unlinkSync('./../backend/'+result[0].pic_one,function(err){
                    console.log(err);
                  }); 
                }

                if (result[0].pic_one && fs.existsSync('./../frontend/'+result[0].pic_one)) {
                  fs.unlinkSync('./../frontend/'+result[0].pic_one,function(err){
                    console.log(err);
                  }); 
                }
              }else{
                file_name = result[0].pic_one;
              }

              if(req.files.pic_two){
                if (result[0].pic_two && fs.existsSync('./../backend/'+result[0].pic_two)) {
                  fs.unlinkSync('./../backend/'+result[0].pic_two,function(err){
                    console.log(err);
                  }); 
                }

                if (result[0].pic_two && fs.existsSync('./../frontend/'+result[0].pic_two)) {
                  fs.unlinkSync('./../frontend/'+result[0].pic_two,function(err){
                    console.log(err);
                  }); 
                }
              }else{
                  file_name1 = result[0].pic_two;
              }

              if(req.files.pic_three){
                if (result[0].pic_three && fs.existsSync('./../backend/'+result[0].pic_three)) {
                  fs.unlinkSync('./../backend/'+result[0].pic_three,function(err){
                    console.log(err);
                  }); 
                }

                if (result[0].pic_three && fs.existsSync('./../frontend/'+result[0].pic_three)) {
                  fs.unlinkSync('./../frontend/'+result[0].pic_three,function(err){
                    console.log(err);
                  }); 
                }
              }else{
                  file_name2 = result[0].pic_three;
              }

              results=JSON.parse(JSON.stringify(result));
              
              var sql = "UPDATE `real_estate` set header_video =?,pic_one =?,pic_two =?,pic_three =?,content_one =?  WHERE id = ?";
              db.query(sql,[uploads,file_name,file_name1,file_name2,content_one,results[0].id], (err, result) => {
               if (err) {
                 res.status(200).send({ status: false,msg:err.sqlMessage });
               }else{
                 res.status(200).send({ msg: 'Page updated successfully'}); 
               }
             });
            }else{
              let query = "INSERT INTO `real_estate` (page_id,header_video =?,pic_one,pic_two,pic_three,content_one) VALUES ('" + req.body.page_id + "','" +
                  uploads + "','" + file_name + "','" + file_name1 + "','" + file_name2 + "','" + content_one + "')";
                db.query(query, (err, result) => {
                if (err) {
                  res.status(200).send({ msg:err.sqlMessage });
                }else{
                  res.status(200).send({ msg: 'Page updated successfully'}); 
                }
              });
            }
          }
        });
        }else{
          db.query('Select * from real_estate', function(err,result){
          if (err) {  
              res.status(200).send({ msg:err.sqlMessage });
          }else{
              
              if(result.length > 0){
                var sql = "UPDATE `real_estate` set content_one =? WHERE id = ?";
                db.query(sql,[content_one,results[0].id], (err, result) => {
                 if (err) {
                   res.status(200).send({ status: false,msg:err.sqlMessage });
                 }else{
                   res.status(200).send({ msg: 'Page updated successfully'}); 
                 }
               });
              }else{
                let query = "INSERT INTO `real_estate` (page_id,content_one) VALUES ('" + req.body.page_id + "','" + content_one + "')";
                db.query(query, (err, result) => {
                if (err) {
                  res.status(200).send({ msg:err.sqlMessage });
                }else{
                  res.status(200).send({ msg: 'Page updated successfully'}); 
                }
              });
              }
            } 
          });  
      } 
  });

  app.post('/api_slide',  function (req, res) {
      var caption = req.body.caption;
      var link = req.body.link;

      if(req.files){
        console.log(req.files,'files');
        let uni = new Date().getTime();

        uploads = '';
        if(req.files.uploads){
          let sampleFile_uploads = req.files.uploads;
          uploads = uni+'_'+req.files.uploads.name.replace(/ /g,"_");
          sampleFile_uploads.mv('./../backend/assets/page/'+uploads);
          sampleFile_uploads.mv('./../frontend/assets/page/'+uploads);
        }
      }else{
        res.status(200).send({ msg: 'No image selected',status:false});
      }
        
      let query = "INSERT INTO `sliders` (image,caption) VALUES ('" + uploads + "','" + caption + "')";
        db.query(query, (err, result) => {
        if (err) {
          res.status(200).send({ msg:err.sqlMessage,status:false });
        }else{
          res.status(200).send({ msg: 'Page updated successfully',status:true}); 
        }
      });
  });

  app.post('/api_slide_update',  function (req, res) {
      var caption = req.body.caption;
      var uploads = req.body.current_image;
      var link = req.body.link;

      if(req.files){
        let uni = new Date().getTime();

        
        if(req.files.uploads){
          let sampleFile_uploads = req.files.uploads;
          uploads = uni+'_'+req.files.uploads.name.replace(/ /g,"_");
          sampleFile_uploads.mv('./../backend/assets/page/'+uploads);
          sampleFile_uploads.mv('./../frontend/assets/page/'+uploads);
        }

        if(req.files.uploads){
          if (fs.existsSync('./../backend/'+req.body.current_image)) {
            fs.unlinkSync('./../backend/'+req.body.current_image,function(err){
              console.log(err);
            }); 
          }

          if (fs.existsSync('./../frontend/'+req.body.current_image)) {
            fs.unlinkSync('./../frontend/'+req.body.current_image,function(err){
              console.log(err);
            }); 
          }
        }
      }
        
      var sql = "UPDATE `sliders` set caption =?,image =?,link =? WHERE id = ?";
      db.query(sql,[caption,uploads,link,results[0].id], (err, result) => {
       if (err) {
         res.status(200).send({ status: false,msg:err.sqlMessage });
       }else{
         res.status(200).send({ status: true,msg: 'Slide updated successfully'}); 
       }
     });
  });

  app.post('/get_homepage_video', function (req, res) {
    db.query('Select * from hompeage_content', function(err,result){
      if (err) {  
        res.status(200).send({ page: result,error:err });
      }else{
        results=JSON.parse(JSON.stringify(result));
        res.status(200).send({ homepage_video: results[0] });
      }
    }); 
  });

  app.delete('/slide/:id', function (req, res) {
    db.query('delete from sliders where id='+req.params.id, function(err,result){
      if (err) {  
        res.status(200).send({ status:false,msg:err.sqlMessage });
      }else{
        res.status(200).send({ status:true,msg: 'Slider Deleted successfully.' });
      }
    }); 
  });

  app.get('/get_slide_info/:id', function (req, res) {
    //console.log(req,'req');

    db.query('Select * from sliders where id='+req.params.id, function(err,result){
      if (err) {  
        res.status(200).send({ page: result,error:err });
      }else{
        results=JSON.parse(JSON.stringify(result));
        res.status(200).send({ homepage_video: results[0] });
      }
    }); 
  });

  app.post('/get_rentals', function (req, res) {
    db.query('Select * from rentals limit 1', function(err,result){
      if (err) {  
        res.status(200).send({ page: result,error:err });
      }else{
        results=JSON.parse(JSON.stringify(result));
        res.status(200).send({ traveldesign: results[0] });
      }
    }); 
  });

  app.post('/get_traveldesign', function (req, res) {
    db.query('Select * from traveldesign limit 1', function(err,result){
      if (err) {  
        res.status(200).send({ page: result,error:err });
      }else{
        results=JSON.parse(JSON.stringify(result));
        res.status(200).send({ traveldesign: results[0] });
      }
    }); 
  });

  app.post('/get_settings', function (req, res) {
    db.query('Select * from settings limit 1', function(err,result){
      if (err) {  
        res.status(200).send({ page: result,error:err });
      }else{
        results=JSON.parse(JSON.stringify(result));
        res.status(200).send({ traveldesign: results[0] });
      }
    }); 
  });

  app.post('/get_lifestyle', function (req, res) {
    db.query('Select * from life_style limit 1', function(err,result){
      if (err) {  
        res.status(200).send({ page: result,error:err });
      }else{
        results=JSON.parse(JSON.stringify(result));
        res.status(200).send({ traveldesign: results[0] });
      }
    }); 
  });

  app.post('/get_real_estate', function (req, res) {
    db.query('Select * from real_estate limit 1', function(err,result){
      if (err) {  
        res.status(200).send({ page: result,error:err });
      }else{
        results=JSON.parse(JSON.stringify(result));
        res.status(200).send({ traveldesign: results[0] });
      }
    }); 
  });


  app.get('/get_pages', function (req, res) {
    db.query('Select * from page order by id asc', function(err,result){
      if (err) {  
        res.status(200).send({ pages: result,error:err });
      }else{
        res.status(200).send({ pages: result,error:'' });
      }
    }); 
  });

  app.get('/get_slider', function (req, res) {
    db.query('Select * from sliders order by id desc', function(err,result){
      if (err) {  
        res.status(200).send({ pages: result,error:err });
      }else{
        res.status(200).send({ pages: result,error:'' });
      }
    }); 
  });

  app.get('/get_page/:id', function (req, res){
      let query = "Select * from page where id = ('" + req.params.id + "')";
      
      db.query(query, function(err,result){
        res.status(200).send({ page: result,error:err });
      }); 
     
  }); 

  app.post('/save_page', function (req, res) {
     console.log(req.body)

     let query = "INSERT INTO `page`  (name,content,status) VALUES ('" + req.body.page_name + "','" +
      req.body.page_content + "','" + req.body.page_status + "')";
      db.query(query, (err, result) => {
        
        console.log(err,'err');

      if (err) {
        res.status(200).send({ status: false,msg:err });
      }else{
        res.status(200).send({ status: true });
      }
    }); 
  });

  app.post('/update_status', function (req, res) {
    var sql = "UPDATE `page` set status =?  WHERE id = ?";

    db.query(sql,[req.body.status,req.body.id], (err, result) => {
       
       console.log(err,'err');

     if (err) {
       res.status(200).send({ status: false,msg:err });
     }else{
       res.status(200).send({ status: true });
     }
   }); 
 });
  
  app.post('/update_page', function (req, res) {
    var sql = "UPDATE `page` set name =? , content =?, status =?, url =?  WHERE id = ?";
    let status = req.body.page_status == 'Active' ? 1 : 0;

    db.query(sql,[req.body.page_name,req.body.page_content,status,req.body.page_link,req.body.id], (err, result) => {
       
       console.log(err,'err');

     if (err) {
       res.status(200).send({ status: false,msg:err });
     }else{
       res.status(200).send({ status: true });
     }
   }); 
 });
};