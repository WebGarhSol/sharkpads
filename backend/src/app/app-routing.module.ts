// Angular
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// Components
import { BaseComponent } from './views/theme/base/base.component';
import { ErrorPageComponent } from './views/theme/content/error-page/error-page.component';
// Auth
import { AuthGuard } from './core/auth';

const routes: Routes = [
	{ path: 'auth', loadChildren: () => import('../app/views/pages/auth/auth.module').then(m => m.AuthModule) },

	{
		path: '',
		component: BaseComponent,
		canActivate: [AuthGuard],
		children: [
			{
				path: '',
				loadChildren: () => import('../app/views/theme/content/builder/builder.module').then(m => m.BuilderModule),
			},
			{
				path: 'businesses',
				loadChildren: () => import('../app/views/pages/businesses/businesses.component').then(m => m.BusinessesComponent),
			},
			{
				path: 'mail',
				loadChildren: () => import('../app/views/pages/apps/mail/mail.module').then(m => m.MailModule),
			},
			// {
			// 	path: 'addevent',
			// 	loadChildren: () => import('../app/views/pages/my-page/my-page.component').then(m => m.MyPageComponent),
			// },
			{
				path: 'ecommerce',
				loadChildren: () => import('../app/views/pages/apps/e-commerce/e-commerce.module').then(m => m.ECommerceModule),
			},
			{
				path: 'ngbootstrap',
				loadChildren: () => import('../app/views/pages/ngbootstrap/ngbootstrap.module').then(m => m.NgbootstrapModule),
			},
			{
				path: 'material',
				loadChildren: () => import('../app/views/pages/material/material.module').then(m => m.MaterialModule),
			},
			{
				path: 'user-management',
				loadChildren: () => import('../app/views/pages/user-management/user-management.module').then(m => m.UserManagementModule),
			},
			{
				path: 'real_estate-management',
				loadChildren: () => import('../app/views/pages/real_estate-management/real_estate-management.module').then(m => m.RealEstateManagementModule),
			},
			{
				path: 'lifestyle-management',
				loadChildren: () => import('../app/views/pages/lifestyle-management/lifestyle-management.module').then(m => m.LifeStyleManagementModule),
			},
			{
				path: 'traveldesign-management',
				loadChildren: () => import('../app/views/pages/traveldesign-management/traveldesign-management.module').then(m => m.TravelDesignManagementModule),
			},
			{
				path: 'inquire-management',
				loadChildren: () => import('../app/views/pages/inquire-management/inquire-management.module').then(m => m.InquireManagementModule),
			},
			{
				path: 'slider-management',
				loadChildren: () => import('../app/views/pages/slider-management/slider-management.module').then(m => m.SliderManagementModule),
			},
			{
				path: 'rentals-management',
				loadChildren: () => import('../app/views/pages/rentals-management/rentals-management.module').then(m => m.RentalsManagementModule),
			},
			{
				path: 'settings-management',
				loadChildren: () => import('../app/views/pages/settings-management/settings-management.module').then(m => m.SettingsManagementModule),
			},
			{
				path: 'homepage-management',
				loadChildren: () => import('../app/views/pages/homepage-management/homepage-management.module').then(m => m.HomepageManagementModule),
			},
			{
				path: 'wizard',
				loadChildren: () => import('../app/views/pages/wizard/wizard.module').then(m => m.WizardModule),
			},
			{
				path: 'builder',
				loadChildren: () => import('../app/views/theme/content/builder/builder.module').then(m => m.BuilderModule),
			},
			{
				path: 'agents',
				loadChildren: () => import('../app/views/pages/agents/agents-routing.module').then(m => m.AgentsRoutingModule),
			},
			{
				path: 'add-user',
				loadChildren: () => import('./add-users/add-users.component').then(m => m.AddUsersComponent),
			},
			{
				path: 'error/403',
				component: ErrorPageComponent,
				data: {
					type: 'error-v6',
					code: 403,
					title: '403... Access forbidden',
					desc: 'Looks like you don\'t have permission to access for requested page.<br> Please, contact administrator',
				},
			},
			{ path: 'error/:type', component: ErrorPageComponent },
			{ path: '', redirectTo: 'dashboard', pathMatch: 'full' },
			{ path: '**', redirectTo: 'dashboard', pathMatch: 'full' },
		],
	},

	{ path: '**', redirectTo: 'error/403', pathMatch: 'full' },
];

@NgModule({
	imports: [
		RouterModule.forRoot(routes),
	],
	exports: [RouterModule],
})
export class AppRoutingModule {
}
