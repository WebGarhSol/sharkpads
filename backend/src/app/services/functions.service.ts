import { Injectable } from '@angular/core';
import { DatePipe } from '@angular/common';

import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class FunctionsService {

  constructor(
    private datePipe: DatePipe
  ) { }

  currentDate() {
    return this.datePipe.transform(new Date(), 'yyyy-MM-dd h:mm:ss');
  }

  presentAlert(title, message = '') {
    Swal.fire(title, message, 'success');
  }

  presentAlertError(title, message = '') {
    Swal.fire(title, message, 'error');
  }

}