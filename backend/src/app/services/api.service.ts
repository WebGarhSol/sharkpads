import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

<<<<<<< HEAD
  //end_point = "http://localhost:3000";
  //end_point = "https://apptest.webgarh.net:3000";
  //end_point = "https://thesharkpad.com:3000";
  end_point = "https://sharkpads.com:3000";
=======
  end_point = "http://localhost:3000";
  //end_point = "https://apptest.webgarh.net:3000";
  //end_point = "https://thesharkpad.com:3000";
  //end_point = "https://sharkpads.com:3000";
>>>>>>> 8984940b6fd992eef6756565fbc19ce2057b00d1

  constructor(private http: HttpClient,
  ) { }


  post(api, body): Observable<any> {
    return this.http.post(`${this.end_point}/${api}`, body);
  }
  get(api): Observable<any> {
    return this.http.get(`${this.end_point}${api}`);
  }

  put(api, body): Observable<any> {
    return this.http.put(`${this.end_point}/${api}`, body);
  }

  delete(api): Observable<any> {
    return this.http.delete(`${this.end_point}/${api}`);
  }

}