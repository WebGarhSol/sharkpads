import { Injectable } from '@angular/core';
import { StoreService } from "../services/store.service";
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  is_login = false;

  constructor(
    private store : StoreService
  ) {
    if(this.getLogin()) {
      this.is_login = true;
    } else {
      this.is_login = false;
    }
   }

  ngOnInit() {
    
  }

  saveLogin(data) {
    localStorage.setItem("cot20token", data.store_owner_id);

    this.store.authusercachedata = data;
    this.is_login = true;
  }

  getLogin() {
    if(this.store.authusercachedata) {
      return this.store.authusercachedata;
    } else {
      return false;
    }
  }

  logOut() {
    localStorage.clear();
    this.is_login = false;
  }

  getToken() {
    if(localStorage.getItem('cot20token')) {
      return localStorage.getItem('cot20token');
    } else {
      return false;
    }
  }
}