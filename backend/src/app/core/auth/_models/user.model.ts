import { BaseModel } from '../../_base/crud';
import { Address } from './address.model';
import { SocialNetworks } from './social-networks.model';

export class User extends BaseModel {
    id: number;
    username: string;
    password: string;
    email: string;
    accessToken: string;
    refreshToken: string;
    roles: number[];
    pic: string;
    fullname: string;
    occupation: string;
	companyName: string;
	phone: string;
    address: Address;
    socialNetworks: SocialNetworks;
    content_two: any;
    content_one: any;
    content_three: any;
    content_four: any;
    totalAngularPackage: any;
    traveldesign: any;
    msg: any;
    name: any;
    value: any;
    linkedin: any;
    facebook: any;
    instgram: any;
    twitter: any;
    aboutus: any;
    common_content: any;
    map: any;
    inquire_email: any;
    tiktok: any;
    airnb: any;
<<<<<<< HEAD
    sharkpads_way: any;

    personal_portfolio_text: any;
	technology_text: any;
	community_relations_text: any;
	identification_text: any;
	contract_and_negotitations_text: any;
	industry_insight_text: any;
	partner_text: any;
	security_text: any;
	sharkpads_way_text: any;
	revenue_management_reporting_text: any;
    marketing_and_branding_text: any;
    page_title: any;
    gallery_text: any;
    amazon: any;
    
    clear(): void {
        this.amazon = '';
        this.gallery_text = '';
        this.page_title = '';
        this.personal_portfolio_text = '';
        this.technology_text = '';
        this.community_relations_text = '';
        this.identification_text = '';
        this.contract_and_negotitations_text = '';
        this.industry_insight_text = '';
        this.partner_text = '';
        this.security_text = '';
        this.sharkpads_way_text = '';
        this.revenue_management_reporting_text = '';
        this.marketing_and_branding_text = '';

        this.sharkpads_way = '';
=======
    clear(): void {
>>>>>>> 8984940b6fd992eef6756565fbc19ce2057b00d1
        this.airnb = '';
        this.tiktok = '';
        this.inquire_email = '';
        this.map = '';
        this.aboutus = '';
        this.common_content = '';
        this.facebook = '';
        this.instgram = '';
        this.twitter = '';
        this.linkedin = '';
        this.id = undefined;``
        this.traveldesign = '';
        this.value = '';
        this.username = '';
        this.msg = '';
        this.name = '';
        this.totalAngularPackage = '';
        this.content_two = '';
        this.content_one = '';
        this.content_three = '';
        this.content_four = '';
        
        this.password = '';
        this.email = '';
        this.roles = [];
        this.fullname = '';
        this.accessToken = 'access-token-' + Math.random();
        this.refreshToken = 'access-token-' + Math.random();
        this.pic = './assets/media/users/default.jpg';
        this.occupation = '';
        this.companyName = '';
        this.phone = '';
        this.address = new Address();
        this.address.clear();
        this.socialNetworks = new SocialNetworks();
        this.socialNetworks.clear();
    }
}
