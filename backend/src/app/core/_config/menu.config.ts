export class MenuConfig {
	public defaults: any = {
		header: {
			self: {},
			items: [
			]
		},
		aside: {
			self: {},
			items: [
				{
					title: 'Manage Pages',
					root: true,
					icon: 'flaticon2-expand',
					page: '/builder'
				},
				{
					title: 'Manage Slide',
					root: true,
					bullet: 'dot',
					icon: 'flaticon2-user-outline-symbol',
					page: '/user-management/users'
				},
				{
					title: 'Manage Settings',
					root: true,
					icon: 'flaticon2-expand',
					page: '/settings-management/homepage/edit'
				}
				/*				
				{
				 	title: 'Manage Agents',
				 	root: true,
				 	bullet: 'dot',
				 	icon: 'flaticon2-user',
				 	page: '/ecommerce/products'
				}*/
			]
		}
	};

	public get configs(): any {
		return this.defaults;
	}
}
