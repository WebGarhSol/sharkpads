import { ChangeDetectorRef, Component, OnInit, ViewChild,ViewEncapsulation } from "@angular/core";
import { NgForm } from "@angular/forms";
import {
	LayoutConfigModel,
	LayoutConfigService,
} from "../../../../core/_base/layout";

import { AuthService } from "../../../../core/auth";

import { Router, NavigationExtras } from '@angular/router';
@Component({
	selector: "kt-builder",
	templateUrl: "./builder.component.html",
	styleUrls: ["./builder.component.scss"],
	encapsulation: ViewEncapsulation.None,
})
export class BuilderComponent implements OnInit {
	storedata: any = [];
	model: LayoutConfigModel;
	loading = false;
	@ViewChild("form", { static: true }) form: NgForm;

	constructor(
		private layoutConfigService: LayoutConfigService,
		private cdr: ChangeDetectorRef,
		private auth: AuthService,
		private router: Router
	) {
		this.getBusiness();
	}

	ngOnInit(): void {
		this.model = this.layoutConfigService.getConfig();
	}

	resetPreview(e: Event): void {
		e.preventDefault();
		this.layoutConfigService.resetConfig();
		location.reload();
	}

	submitPreview(e: Event): void {
		this.layoutConfigService.setConfig(this.model, true);
		location.reload();
	}
	location(a) {
		alert(a);
	}

	getting: Boolean;
	getBusiness() {
		this.getting = true;
		this.auth.get(`get_pages`).subscribe((result) => {
			this.getting = false;
			// debugger;
			this.storedata = result.pages;
			this.cdr.detectChanges();
		});
	}
	Delete(id) {
		var id = id._id
		this.getting = true;
		this.auth.delete(`event/` + id).subscribe((result) => {
			this.getting = false;
			// debugger;
			this.getBusiness();
			this.storedata = result;
			this.cdr.detectChanges();
		});
	}
	Archive(id ,status) {
		var data = {
			id: id,
			status: status
		}
		debugger;
		this.getting = true;
		this.auth.post(`update_status`, data).subscribe((result) => {
			this.getting = false;
			debugger;
			this.getBusiness();
			this.cdr.detectChanges();
		});
	}
	Close(id) {
		var data = {
			id: id._id,
			close: true
		}
		debugger;
		this.getting = true;
		this.auth.put(`close`, data).subscribe((result) => {
			this.getting = false;
			// debugger;
			this.getBusiness();
			this.cdr.detectChanges();
		});
	}
	UnArchive(id ,status) {
		var data = {
			id: id,
			archive: status
		}
		debugger;
		this.getting = true;
		this.auth.put(`archive`, data).subscribe((result) => {
			this.getting = false;
			debugger;
			this.getBusiness();
			this.cdr.detectChanges();
		});
	}
	Open(id) {
		var data = {
			id: id._id,
			close: false
		}
		debugger;
		this.getting = true;
		this.auth.put(`close`, data).subscribe((result) => {
			this.getting = false;
			// debugger;
			this.getBusiness();
			this.cdr.detectChanges();
		});
	}
	Edit(id) {
		console.log(id,'id')
		let navigationExtras: NavigationExtras = {
			queryParams: {
				special: id
			}
		};
		
		console.log(navigationExtras,'navigationExtras',this.router.navigate(['/ecommerce']));

		this.router.navigate(['/ecommerce'], navigationExtras);
	}

	ManageSection(url,id) {
		let navigationExtras: NavigationExtras = {
			queryParams: {
				special: id
			}
		};

		this.router.navigate([url], navigationExtras);
	}
}