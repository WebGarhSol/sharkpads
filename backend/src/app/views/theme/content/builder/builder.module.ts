// Angular
import { NgModule } from '@angular/core';
// import {ChangeDetectorRef, Input, OnChanges} from "@angular/core";
// import {ViewContainerRef, ChangeDetectorRef, AfterContentChecked } from '@angular/core';

import { Inject, ChangeDetectionStrategy} from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { MatTabsModule } from '@angular/material';
// NgBootstrap
import { NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
// Perfect Scrollbar
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
// Partials
import { PartialsModule } from '../../../partials/partials.module';
// Highlight JS
import { HighlightModule } from 'ngx-highlightjs';
// CoreModule
import { CoreModule } from '../../../../core/core.module';
// Builder component
import { BuilderComponent } from './builder.component';
import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';

import { AuthNoticeService, AuthService, Login } from '../../../../core/auth';
@NgModule({
	imports: [
		CommonModule,
		PartialsModule,
		FormsModule,
		MatTabsModule,
		CoreModule,
		PerfectScrollbarModule,
		HighlightModule,
		RouterModule.forChild([
			{
				path: '',
				component: BuilderComponent
			}
		]),

		// ng-bootstrap modules
		NgbTabsetModule,
	],
	providers: [],
	declarations: [BuilderComponent]
})
export class BuilderModule {
	data:any='afjns';
	storedata:any=[
		{"id":1,name:'Ro'}
	];
	loading = false;
	
	constructor(
		private auth: AuthService,
		private authNoticeService: AuthNoticeService,
		// private cdr: ChangeDetectorRef,
	
	) {
	}
	Date=new Date();

	
	


}