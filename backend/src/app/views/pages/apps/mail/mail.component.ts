import { Component } from "@angular/core";
import { AuthService } from "../../../../core/auth";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import {
	ChangeDetectorRef,
	ViewEncapsulation,
} from "@angular/core";

@Component({
	selector: 'kt-mail',
	templateUrl: './mail.component.html',
})
export class MailComponent { 
	loginForm: FormGroup;
	loading = false;
	storedata: any = [];
	constructor(
		private auth: AuthService,
		private fb: FormBuilder,
		private cdr: ChangeDetectorRef,
		private router: Router,
	) {
		this.getBusiness();
	 }

	 initLoginForm() {
		this.loginForm = this.fb.group({
			event: [
				"",
				Validators.compose([
					Validators.required,
					Validators.minLength(3),
					Validators.maxLength(320),
				]),
			],
			url: [
				"",
				Validators.compose([
					Validators.required,
					Validators.minLength(3),
					Validators.maxLength(100),
				]),
			],
			host: [
				"",
				Validators.compose([
					Validators.required
				]),
			],
		});
	}
	submit() {
		const controls = this.loginForm.controls;
		if (this.loginForm.invalid) {
			Object.keys(controls).forEach((controlName) =>
				controls[controlName].markAsTouched()
			);
			return;
		}

		this.loading = true;
		// debugger;
		const authData = {
			host_id: controls.host.value,
			event_name: controls.event.value,
			slug_name: controls.url.value,
			created_at: new Date(),
			lastupdated_at: new Date(),
		};
		debugger;
		this.auth.post("event", authData).subscribe((result) => {
			// debugger;
			alert("Event added succesfully");
			this.router.navigateByUrl("/builder");
			console.log(result);
		}, error => {
			console.log(error)
		});

	}
	isControlHasError(controlName: string, validationType: string): boolean {
		const control = this.loginForm.controls[controlName];
		if (!control) {
			return false;
		}

		const result =
			control.hasError(validationType) &&
			(control.dirty || control.touched);
		return result;
	}
	getting : Boolean;
	getBusiness() {
		this.getting = true;
		this.auth.get(`user`).subscribe((result) => {
			this.getting = false;
			debugger;
			this.storedata = result;
			this.cdr.detectChanges();
		});
	}
	
}