// Angular
import { Component, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AuthNoticeService, AuthService, Login } from "../../../../../../core/auth";
import { ActivatedRoute, Router } from "@angular/router";
import {
	ChangeDetectorRef,
	ViewEncapsulation,
} from "@angular/core";
@Component({
	selector: 'kt-orders-list',
	templateUrl: './orders-list.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class OrdersListComponent {
	loginForm: FormGroup;
	loading = false;
	storedata: any = [];
	events: any = [];
	data: any = [];
	UserData: any = {};
	isEnabled: boolean = false;
	update: any = "Add";
	pic_one: any;
	current_image: any;
	
	constructor(
		private auth: AuthService,
		private fb: FormBuilder,
		private cdr: ChangeDetectorRef,
		private router: Router,
		private route: ActivatedRoute
	) {
		this.getBusiness();
		this.route.queryParams.subscribe(params => {
			if (params && params.special) {
				// debugger;
				var data = JSON.parse(params.special);
				this.data.push(JSON.parse(params.special));
				this.update = "Update";
				// debugger;
				this.GetUserDetails(data);
			}
		});
	}


	ngOnInit(): void {
		this.initLoginForm();
	}
	initLoginForm() {
		this.loginForm = this.fb.group({
			caption: [
				"",
				Validators.compose([
					Validators.required
				]),
			],
			link: [
				"",
				Validators.compose([]),
			]

		});
	}

	pic_one_fun(element) {
		this.pic_one = element.target.files;
	}
  

	// ngOnDestroy() {
	// 	this.subscriptions.forEach(el => el.unsubscribe());
	// }
	submit() {
		const controls = this.loginForm.controls;
		if (this.loginForm.invalid) {
			Object.keys(controls).forEach((controlName) =>
				controls[controlName].markAsTouched()
			);
			return;
		}
		
		if (this.update == "Update") {
			this.UpdateSubmit();
			return;
		}
		this.loading = true;
		// debugger;
		const authData = {
			caption: controls.caption.value
		};

		let formData = new FormData();
		if(this.pic_one){
			for (var i = 0; i < this.pic_one.length; i++) {
				formData.append("uploads", this.pic_one[i], this.pic_one[i].name);
			}
		}

	    formData.append("caption", controls.caption.value);
		formData.append("link", controls.link.value);

		this.auth.post("api_slide", formData).subscribe((result:any) => {
			alert(result.msg);
			if(result.status){
				this.router.navigateByUrl("/user-management/users");
			}
		}, error => {
			alert(error)
		});
	}

	UpdateSubmit() {
		const controls = this.loginForm.controls;
		if (this.loginForm.invalid) {
			Object.keys(controls).forEach((controlName) =>
				controls[controlName].markAsTouched()
			);
			return;
		}
		
		this.loading = true;
		let formData = new FormData();
		if(this.pic_one){
			for (var i = 0; i < this.pic_one.length; i++) {
				formData.append("uploads", this.pic_one[i], this.pic_one[i].name);
			}
		}

		formData.append("caption", controls.caption.value);
		formData.append("link", controls.link.value);
		formData.append("current_image", this.current_image);
		this.auth.post("api_slide_update", formData).subscribe((result:any) => {
			alert(result.msg);
			if(result.status){
				this.router.navigateByUrl("/user-management/users");
			}
		}, error => {
			alert(error)
		});
		
	}

	isControlHasError(controlName: string, validationType: string): boolean {
		const control = this.loginForm.controls[controlName];
		if (!control) {
			return false;
		}

		const result =
			control.hasError(validationType) &&
			(control.dirty || control.touched);
		return result;
	}
	getting: Boolean;
	getBusiness() {
		this.getting = true;
		this.auth.get(`event`).subscribe((result) => {
			this.getting = false;
			// debugger;
			this.events = result;
			this.cdr.detectChanges();
		});
	}
	GetUserDetails(id) {
		// debugger;
		var idd = id;
	
		this.auth.get("get_slide_info/" + idd).subscribe(
			(result: any) => {
				// debugger;
				console.log(result,'result')
				this.loginForm.get("caption").setValue(result.homepage_video.caption);
				this.loginForm.get("link").setValue(result.homepage_video.link);
				this.current_image = result.homepage_video.image;
			},
			(error) => {
				debugger;
				console.log(error)
				//error = error.json();
			}
		);
	}
}
