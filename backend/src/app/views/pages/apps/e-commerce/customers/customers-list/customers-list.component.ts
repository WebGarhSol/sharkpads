// Angular
import { Component, OnInit, ElementRef, ViewChild, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
// Material
import { SelectionModel } from '@angular/cdk/collections';
import { MatPaginator, MatSort, MatSnackBar, MatDialog } from '@angular/material';
// RXJS
import { debounceTime, distinctUntilChanged, tap, skip, delay, take } from 'rxjs/operators';
import { fromEvent, merge, Subscription, of } from 'rxjs';
// Translate Module
import { TranslateService } from '@ngx-translate/core';
// NGRX
import { Store, ActionsSubject } from '@ngrx/store';
import { AppState } from '../../../../../../core/reducers';

import { ActivatedRoute, Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
// CRUD
import { LayoutUtilsService, MessageType, QueryParamsModel } from '../../../../../../core/_base/crud';
// Services and Models
import { CustomerModel, CustomersDataSource, CustomersPageRequested, OneCustomerDeleted, ManyCustomersDeleted, CustomersStatusUpdated } from '../../../../../../core/e-commerce';
// Components

// import { AuthNoticeService, AuthService, Login } from "../../../../core/auth";
import { AuthNoticeService, AuthService, Login } from "../../../../../../core/auth";
import { CustomerEditDialogComponent } from '../customer-edit/customer-edit.dialog.component';

//import { CKEditorModule } from 'ckeditor4-angular'
//import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
//import { ChangeEvent } from '@ckeditor/ckeditor5-angular/ckeditor.component';
//import * as ClassicEditorTinymce from '@tinymce/tinymce-angular';

declare var tinymce: any;

import {
	ChangeDetectorRef,
	ViewEncapsulation,
} from "@angular/core";
// Table with EDIT item in MODAL
// ARTICLE for table with sort/filter/paginator
// https://blog.angular-university.io/angular-material-data-table/
// https://v5.material.angular.io/compgetItemCssClassByStatusonents/table/overview
// https://v5.material.angular.io/components/sort/overview
// https://v5.material.angular.io/components/table/overview#sorting
// https://www.youtube.com/watch?v=NSt9CI3BXv4
@Component({
	// tslint:disable-next-line:component-selector
	selector: 'kt-customers-list',
	templateUrl: './customers-list.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,

})
export class CustomersListComponent implements OnInit, OnDestroy {
	// Table fields
	loginForm: FormGroup;
	current_status: any;
	
	
	//Editor = ClassicEditorTinymce;
	loading = false;
	storedata: any = [];
	dataSource: CustomersDataSource;
	events: any = [];
	data: any = [];
	UserData: any = {};
	isEnabled: boolean = false;
	update: any = "Add";
	displayedColumns = ['select', 'id', 'lastName', 'firstName', 'email', 'gender', 'status', 'type', 'actions'];
	@ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
	@ViewChild('sort1', { static: true }) sort: MatSort;
	// Filter fields
	@ViewChild('searchInput', { static: true }) searchInput: ElementRef;
	filterStatus = '';
	filterType = '';
	// Selection
	selection = new SelectionModel<CustomerModel>(true, []);
	customersResult: CustomerModel[] = [];
	// Subscriptions
	private subscriptions: Subscription[] = [];

	/**
	 * Component constructor
	 *
	 * @param dialog: MatDialog
	 * @param snackBar: MatSnackBar
	 * @param layoutUtilsService: LayoutUtilsService
	 * @param translate: TranslateService
	 * @param store: Store<AppState>
	 */
	constructor(
		public dialog: MatDialog,
		private auth: AuthService,
		public snackBar: MatSnackBar,
		private layoutUtilsService: LayoutUtilsService,
		private translate: TranslateService,
		private store: Store<AppState>,
		private fb: FormBuilder,
		private cdr: ChangeDetectorRef,
		private router: Router,
		private route: ActivatedRoute
	) {
		this.getBusiness();
		this.route.queryParams.subscribe(params => {
			if (params && params.special) {
				// debugger;
				var data = JSON.parse(params.special);
				this.data.push(JSON.parse(params.special));
				this.update = "Update";
				debugger;
				this.GetEventDetails(data);
			}
		});
	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */

	ngOnInit(): void {
		this.initLoginForm();
		
		/*
		tinymce.init(
			{
				plugins: 'pageembed code preview',
  				selector: "#page_content",
				menubar: 'file edit view insert format tools table tc help',
  				toolbar: 'pageembed code preview | undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist checklist | forecolor backcolor casechange permanentpen formatpainter removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media pageembed template link anchor codesample | a11ycheck ltr rtl | showcomments addcomment',
				autosave_ask_before_unload: true,
				height: 100,
				image_caption: true,
				quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
				noneditable_noneditable_class: 'mceNonEditable',
				toolbar_mode: 'sliding',
				spellchecker_whitelist: ['Ephox', 'Moxiecode'],
				tinycomments_mode: 'embedded',
				content_style: '.mymention{ color: gray; }',
				contextmenu: 'link image imagetools table configurepermanentpen',
				a11y_advanced_options: true,
				importcss_append: true,
				templates: [
					{ title: 'New Table', description: 'creates a new table', content: '<div class="mceTmpl"><table width="98%%"  border="0" cellspacing="0" cellpadding="0"><tr><th scope="col"> </th><th scope="col"> </th></tr><tr><td> </td><td> </td></tr></table></div>' },
					{ title: 'Starting my story', description: 'A cure for writers block', content: 'Once upon a time...' },
					{ title: 'New list with dates', description: 'New List with dates', content: '<div class="mceTmpl"><span class="cdate">cdate</span><br /><span class="mdate">mdate</span><h2>My List</h2><ul><li></li><li></li></ul></div>' }
				],
				template_cdate_format: '[Date Created (CDATE): %m/%d/%Y : %H:%M:%S]',
				template_mdate_format: '[Date Modified (MDATE): %m/%d/%Y : %H:%M:%S]',
				link_list: [
					{ title: 'My page 1', value: 'https://www.tiny.cloud' },
					{ title: 'My page 2', value: 'http://www.moxiecode.com' }
				  ],
				  image_list: [
					{ title: 'My page 1', value: 'https://www.tiny.cloud' },
					{ title: 'My page 2', value: 'http://www.moxiecode.com' }
				  ],
				  image_class_list: [
					{ title: 'None', value: '' },
					{ title: 'Some class', value: 'class-name' }
				  ],
				  allow_script_urls: true,
				  script_url : [
					  'https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js',
					  'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.5/slick.min.js',
					  'http://142.93.215.22/sharkpads/js/script.js',
					  'https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js',
					  'https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js',
					  'http://142.93.215.22/sharkpads/js/custom.js'
				  ],
				  content_css: [
					'https://fonts.googleapis.com/css2?family=Dosis:wght@200;300;400;500;600;700;800&display=swap',
					'https://fonts.googleapis.com/css2?family=Dosis:wght@200;300;400;500;600;700;800&family=Lora:ital,wght@0,400;0,500;0,600;0,700;1,400;1,500;1,600;1,700&display=swap',
					'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css',
					'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.5/slick.min.css',
					'http://142.93.215.22/sharkpads/css/bootstrap.min.css',
					'http://142.93.215.22/sharkpads/css/style.css',
				  ]
			});
		*/
	
	}
	
	initLoginForm() {
		this.loginForm = this.fb.group({
			page_name: [
				"",
				Validators.compose([
					Validators.required,
					Validators.minLength(3),
					Validators.maxLength(320),
				]),
			],
			page_link: [
				"",
				Validators.compose([
					Validators.required
				]),
			]
			/*
			,
			page_content: [
				"",
				Validators.compose([
					Validators.minLength(3),
					Validators.maxLength(320),
				]),
			
			],
			page_status: [
				"",
				Validators.compose([
					Validators.required
				]),
			],
			*/
		});
	}


	/**
	 * On Destroy
	 */
	ngOnDestroy() {
		tinymce.remove();
		this.subscriptions.forEach(el => el.unsubscribe());
	}

	/**
	 * Load Customers List from service through data-source
	 */
	submit() {
		const controls = this.loginForm.controls;
		
		console.log(controls,'controls');

		if (this.loginForm.invalid) {
			Object.keys(controls).forEach((controlName) =>
				controls[controlName].markAsTouched()
			);
			return;
		}
		if (this.update == "Update") {
			this.UpdateSubmit();
			return;
		}

		this.loading = true;
		// debugger;
		const authData = {
			page_link: controls.page_link.value,
			page_name: controls.page_name.value,
			//page_status: controls.page_status.value,
			//page_content: tinymce.activeEditor.getContent( { format : 'html' } )
		};
		// debugger;
		this.auth.post("save_page", authData).subscribe((result) => {
			// debugger;
			alert("Page added succesfully");
			this.router.navigateByUrl("/builder");
			console.log(result);
		}, error => {
			console.log(error)
		});

	}
	isControlHasError(controlName: string, validationType: string): boolean {
		const control = this.loginForm.controls[controlName];
		if (!control) {
			return false;
		}

		const result =
			control.hasError(validationType) &&
			(control.dirty || control.touched);
		return result;
	}
	getting: Boolean;
	getBusiness() {
		this.getting = true;
		this.auth.get(`user`).subscribe((result) => {
			this.getting = false;
			// debugger;
			this.storedata = result;
			this.cdr.detectChanges();
		});

	}
	UpdateSubmit() {
		const controls = this.loginForm.controls;
		if (this.loginForm.invalid) {
			Object.keys(controls).forEach((controlName) =>
				controls[controlName].markAsTouched()
			);
			return;
		}
		this.loading = true;
		const authData = {
			id: this.data[0],
			page_link: controls.page_link.value,
			page_name: controls.page_name.value,
			//page_status: controls.page_status.value,
			//page_content: tinymce.activeEditor.getContent( { format : 'html' } )
		};
		debugger;
		this.auth.post("update_page", authData).subscribe((result) => {
			debugger;
			alert("Page has been updated succesfully");
			this.router.navigateByUrl("/builder");
			console.log(result);
		}, error => {
			console.log(error)
		});
	}
	GetEventDetails(id) {
		// debugger;
		var idd = id;

		this.auth.get("get_page/" + idd).subscribe(
			(result: any) => {
				//debugger;
				this.loginForm.get("page_name").setValue(result.page[0].name);
				this.loginForm.get("page_link").setValue(result.page[0].url);
				//tinymce.activeEditor.setContent(result.page[0].content);
				//let status = result.page[0].status;
				
				//this.current_status = status == 1 ? 'Active':'InActive';
				//this.loginForm.get("page_status").setValue(status);
				this.isEnabled = true;
				this.UserData = result;
			},
			(error) => {
				debugger;
				//error = error.json();
			}
		);
	}
}
