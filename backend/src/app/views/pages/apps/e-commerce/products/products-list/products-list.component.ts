import { Component, ChangeDetectionStrategy,ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ApiService } from "../../../../../../services/api.service";
import { FunctionsService } from "../../../../../../services/functions.service";

@Component({
	selector: 'kt-products-list',
	templateUrl: './products-list.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductsListComponent {

	loginForm: FormGroup;
	action = 1;
	constructor(
		private fb: FormBuilder,
		private api: ApiService,
		public fun: FunctionsService,
		private cdr: ChangeDetectorRef
	) { }

	ngOnInit(): void {
		this.initLoginForm();
		this.get();
	}

	initLoginForm() {
		this.loginForm = this.fb.group({
			email: [
				"",
				Validators.compose([
					Validators.required,
					Validators.minLength(3),
					Validators.maxLength(320),
				]),
			],
			name: [
				"",
				Validators.compose([
					Validators.required,
					Validators.minLength(2),
					Validators.maxLength(100),
				]),
			],
			phone_number: [
				"",
				Validators.compose([
					Validators.required,
					Validators.minLength(12),
					Validators.maxLength(12)
				]),
			],
			status: [
				"Active",
				Validators.compose([
					Validators.required,
				]),
			]
		});
	}

	isControlHasError(controlName: string, validationType: string): boolean {
		const control = this.loginForm.controls[controlName];
		if (!control) {
			return false;
		}

		const result =
			control.hasError(validationType) &&
			(control.dirty || control.touched);
		return result;
	}

	submit() {
		const controls = this.loginForm.controls;
		if (this.loginForm.invalid) {
			Object.keys(controls).forEach((controlName) =>
				controls[controlName].markAsTouched()
			);
			return;
		}
		this.loginForm.value.created_at = this.fun.currentDate();
		this.save();
	}

	submiting: Boolean;
	save() {
		this.submiting = true;
		this.api.post(`agent`, this.loginForm.value).subscribe((result) => {
			this.submiting = false;
			if (result && result.status == "success") {
				this.loginForm.reset();
				this.action = 1;
				this.get();
			} else {
				this.fun.presentAlertError(result.message);
			}
			this.cdr.detectChanges();
		});
	}

	agents = [];
	loading: Boolean;
	get() {
		this.loading = true;
		this.api.get(`agent`).subscribe((result) => {
			this.loading = false;
			this.agents = result;
			this.cdr.detectChanges();
		});
	}

	edit(agent) {
		this.loginForm = this.fb.group(agent);
		this.action = 2;
	}

	actionClick(e) {
		this.action = e;
	}
}