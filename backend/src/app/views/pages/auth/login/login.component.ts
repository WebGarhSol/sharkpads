import {
	ChangeDetectorRef,
	Component,
	OnDestroy,
	OnInit,
	ViewEncapsulation,
} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Observable, Subject } from "rxjs";
import { finalize, takeUntil, tap } from "rxjs/operators";
import { TranslateService } from "@ngx-translate/core";
import { Store } from "@ngrx/store";
import { AppState } from "../../../../core/reducers";
import { AuthNoticeService, AuthService, Login } from "../../../../core/auth";

@Component({
	selector: "kt-login",
	templateUrl: "./login.component.html",
	encapsulation: ViewEncapsulation.None,
})
export class LoginComponent implements OnInit, OnDestroy {
	loginForm: FormGroup;
	loading = false;
	isLoggedIn$: Observable<boolean>;
	errors: any = [];

	private unsubscribe: Subject<any>;

	private returnUrl: any;

	constructor(
		private router: Router,
		private auth: AuthService,
		private authNoticeService: AuthNoticeService,
		private translate: TranslateService,
		private store: Store<AppState>,
		private fb: FormBuilder,
		private cdr: ChangeDetectorRef,
		private route: ActivatedRoute
	) {
		this.unsubscribe = new Subject();
	}

	ngOnInit(): void {
		this.initLoginForm();

		this.route.queryParams.subscribe((params) => {
			this.returnUrl = params.returnUrl || "/";
		});
	}

	ngOnDestroy(): void {
		this.authNoticeService.setNotice(null);
		this.unsubscribe.next();
		this.unsubscribe.complete();
		this.loading = false;
	}

	initLoginForm() {
		this.loginForm = this.fb.group({
			email: [
				"",
				Validators.compose([
					Validators.required,
					Validators.minLength(3),
					Validators.maxLength(320),
				]),
			],
			password: [
				"",
				Validators.compose([
					Validators.required,
					Validators.minLength(3),
					Validators.maxLength(100),
				]),
			],
		});
	}

	submit() {
		const controls = this.loginForm.controls;
		if (this.loginForm.invalid) {
			Object.keys(controls).forEach((controlName) =>
				controls[controlName].markAsTouched()
			);
			return;
		}

		this.loading = true;
			debugger;
		const authData = {
			email: controls.email.value,
			password: controls.password.value,
		};
		const authData2 = {
			username: controls.email.value,
			password: controls.password.value,
		};

		this.auth
					.login(authData.email, authData.password)
					.pipe(
						tap(user => {
							if (user) {
								this.store.dispatch(new Login({ authToken: user.accessToken }));
								this.router.navigateByUrl("/builder");
							} else {
								this.authNoticeService.setNotice(this.translate.instant('AUTH.VALIDATION.INVALID_LOGIN'), 'danger');
							}
						}),
						takeUntil(this.unsubscribe),
						finalize(() => {
							this.loading = false;
							this.cdr.markForCheck();
						})
					)
					.subscribe();
		// this.auth.post(`admin/auth`, authData2).subscribe((result) => {
		// 	this.loading = false;
		// 	if (result && result.status == "success") {
		// 		this.auth
		// 			.login(authData.email, authData.password)
		// 			.pipe(
		// 				tap(user => {
		// 					if (user) {
		// 						this.store.dispatch(new Login({ authToken: user.accessToken }));
		// 						this.router.navigateByUrl("/builder");
		// 					} else {
		// 						this.authNoticeService.setNotice(this.translate.instant('AUTH.VALIDATION.INVALID_LOGIN'), 'danger');
		// 					}
		// 				}),
		// 				takeUntil(this.unsubscribe),
		// 				finalize(() => {
		// 					this.loading = false;
		// 					this.cdr.markForCheck();
		// 				})
		// 			)
		// 			.subscribe();
		// 	} else {
		// 		this.authNoticeService.setNotice(this.translate.instant('AUTH.VALIDATION.INVALID_LOGIN'), 'danger');
		// 	}
		// });
	}

	isControlHasError(controlName: string, validationType: string): boolean {
		const control = this.loginForm.controls[controlName];
		if (!control) {
			return false;
		}

		const result =
			control.hasError(validationType) &&
			(control.dirty || control.touched);
		return result;
	}
}