import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { AuthService } from "../../../../../core/auth";
import { Router, NavigationExtras } from '@angular/router';
import Swal from 'sweetalert2'
@Component({
	selector: 'kt-users-list',
	templateUrl: './users-list.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class UsersListComponent {
	constructor(
		private auth: AuthService,
		private cdr: ChangeDetectorRef,
		private router: Router) {
		this.getBusiness();
	}

	ngOnInit() {
	}

	getting: Boolean;
	storedata = [];
	getBusiness() {
		this.getting = true;
		this.auth.get(`user`).subscribe((result) => {
			this.getting = false;
			this.storedata = result;
			this.cdr.detectChanges();
		});
	}
	ChangeUser(id, type) {
		var data = {
			id: id._id,
			user_type: type
		}
		debugger;
		this.getting = true;
		this.auth.put("user", data).subscribe((result) => {
			this.getting = false;
			debugger;
			this.getBusiness();
			this.cdr.detectChanges();
		});
	}
	ConfirmDelete(id) {
		const swalWithBootstrapButtons = Swal.mixin({
			customClass: {
				confirmButton: 'btn btn-success',
				cancelButton: 'btn btn-danger'
			},
			buttonsStyling: false
		})

		swalWithBootstrapButtons.fire({
			title: 'Are you sure?',
			text: "You won't be able to revert this!",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Yes',
			cancelButtonText: 'cancel',
			reverseButtons: true
		}).then((result) => {
			if (result.isConfirmed) {
				this.Delete(id);
			}
		})
	}

	Delete(id) {
		var id = id._id
		this.getting = true;
		this.auth.delete("user/" + id).subscribe((result) => {
			this.getting = false;
			this.getBusiness();
			this.cdr.detectChanges();
		});
	}
	Edit(id) {
		let navigationExtras: NavigationExtras = {
			queryParams: {
				special: JSON.stringify(id._id)
			}
		};
		this.router.navigate(['/ecommerce/orders'], navigationExtras);
	}
}