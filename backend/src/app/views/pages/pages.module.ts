import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { PartialsModule } from '../partials/partials.module';
import { CoreModule } from '../../core/core.module';
import { MailModule } from './apps/mail/mail.module';
import { ECommerceModule } from './apps/e-commerce/e-commerce.module';
import { UserManagementModule } from './user-management/user-management.module';
import { MyPageComponent } from './my-page/my-page.component';
import { BusinessesComponent } from './businesses/businesses.component';
import { AgentsComponent } from './agents/agents.component';
import { HomepageManagementModule } from './homepage-management/homepage-management.module';
import { TravelDesignManagementModule } from './traveldesign-management/traveldesign-management.module';
import { RealEstateManagementModule } from './real_estate-management/real_estate-management.module';
import { LifeStyleManagementModule } from './lifestyle-management/lifestyle-management.module';
import { InquireManagementModule } from './inquire-management/inquire-management.module';
import { RentalsManagementModule } from './rentals-management/rentals-management.module';
import { SettingsManagementModule } from './settings-management/settings-management.module';
import { SliderManagementModule } from './slider-management/slider-management.module';


@NgModule({
	declarations: [MyPageComponent, BusinessesComponent, AgentsComponent],
	exports: [],
	imports: [
		CommonModule,
		HttpClientModule,
		FormsModule,
		CoreModule,
		PartialsModule,
		MailModule,
		ECommerceModule,
		UserManagementModule,
		HomepageManagementModule,
		TravelDesignManagementModule,
		RealEstateManagementModule,
		LifeStyleManagementModule,
		InquireManagementModule,
		RentalsManagementModule,
		SettingsManagementModule,
		SliderManagementModule
	],
	providers: []
})
export class PagesModule {
}