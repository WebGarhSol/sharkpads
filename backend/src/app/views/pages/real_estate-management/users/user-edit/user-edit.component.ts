// Angular
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// RxJS
import { BehaviorSubject, Observable, of, Subscription } from 'rxjs';
// NGRX
import { Store, select } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { AppState } from '../../../../../core/reducers';
// Layout
import { SubheaderService, LayoutConfigService } from '../../../../../core/_base/layout';
import { LayoutUtilsService, MessageType } from '../../../../../core/_base/crud';
// Services and Models
import { HttpClient } from '@angular/common/http';
import { ApiService } from './../../../../../services/api.service';

import {
	User,
	UserUpdated,
	Address,
	SocialNetworks,
	selectHasUsersInStore,
	selectUserById,
	UserOnServerCreated,
	selectLastCreatedUserId,
	selectUsersActionLoading
} from '../../../../../core/auth';

@Component({
	selector: 'kt-user-edit',
	templateUrl: './user-edit.component.html',
})
export class UserEditComponent implements OnInit, OnDestroy {
	// Public properties
	user: User;
	userId$: Observable<number>;
	oldUser: User;
	selectedTab = 0;
	loading$: Observable<boolean>;
	rolesSubject = new BehaviorSubject<number[]>([]);
	addressSubject = new BehaviorSubject<Address>(new Address());
	soicialNetworksSubject = new BehaviorSubject<SocialNetworks>(new SocialNetworks());
	userForm: FormGroup;
	hasFormErrors = false;
	//end_point = "http://localhost:3000";
	//end_point = "https://apptest.webgarh.net:3000";
	//end_point = "http://139.59.91.66:3000"
	// Private properties
	private subscriptions: Subscription[] = [];
	uploadedFiles: Array < File > ;
	
	pic_one: Array < File > ;
	pic_two: Array < File > ;
	pic_three: Array < File > ;
	personal_portfolio: Array < File > ;
	technology: Array < File > ;
	community_relations: Array < File > ;
	identification: Array < File > ;
	contract_and_negotitations: Array < File > ;
	industry_insight: Array < File > ;
	partner: Array < File > ;
	security: Array < File > ;
	sharkpads_way: Array < File > ;
	revenue_management_reporting: Array < File > ;
	marketing_and_branding: Array < File > ;

	personal_portfolio_text: any;
	technology_text: any;
	community_relations_text: any;
	identification_text: any;
	contract_and_negotitations_text: any;
	industry_insight_text: any;
	partner_text: any;
	security_text: any;
	sharkpads_way_text: any;
	revenue_management_reporting_text: any;
	marketing_and_branding_text: any;
	page_title: any;

	message: any;
	page_id: 1;
	page_data : any;
	/**
	 * Component constructor
	 *
	 * @param activatedRoute: ActivatedRoute
	 * @param router: Router
	 * @param userFB: FormBuilder
	 * @param subheaderService: SubheaderService
	 * @param layoutUtilsService: LayoutUtilsService
	 * @param store: Store<AppState>
	 * @param layoutConfigService: LayoutConfigService
	 */
	constructor(private http: HttpClient, private activatedRoute: ActivatedRoute,
		           private router: Router,
		           private userFB: FormBuilder,
		           private subheaderService: SubheaderService,
				   private layoutUtilsService: LayoutUtilsService,
				   public apiurl: ApiService,
		           private store: Store<AppState>,
		           private layoutConfigService: LayoutConfigService) { }

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	fileChange(element) {
      this.uploadedFiles = element.target.files;
  	}

  	pic_one_fun(element) {
      this.pic_one = element.target.files;
  	}

  	pic_two_fun(element) {
      this.pic_two = element.target.files;
  	} 

  	pic_three_fun(element) {
      this.pic_three = element.target.files;
	}
	  
	pic_chnage(element,forsection) {
		if(forsection=='personal_portfolio'){
			this.personal_portfolio = element.target.files;
		}

		if(forsection=='technology'){
			this.technology = element.target.files;
		}

		if(forsection=='community_relations'){
			this.community_relations = element.target.files;
		}

		if(forsection=='identification'){
			this.identification = element.target.files;
		}

		if(forsection=='contract_and_negotitations'){
			this.contract_and_negotitations = element.target.files;
		}
		
		if(forsection=='industry_insight'){
			this.industry_insight = element.target.files;
		}
		
		if(forsection=='partner'){
			this.partner = element.target.files;
		}

		if(forsection=='security'){
			this.security = element.target.files;
		}

		if(forsection=='marketing_and_branding'){
			this.marketing_and_branding = element.target.files;
		}

		if(forsection=='sharkpads_way'){
			this.sharkpads_way = element.target.files;
		}

		if(forsection=='revenue_management_reporting'){
			this.revenue_management_reporting = element.target.files;
		}
		
		if(forsection=='revenue_management_reporting'){
			this.revenue_management_reporting = element.target.files;
		}
	}  

  	getparam(name){
   	  if(name=(new RegExp('[?&]'+encodeURIComponent(name)+'=([^&]*)')).exec(location.search))
      return decodeURIComponent(name[1]);
	}


	ngOnInit() {
		let api = 'get_real_estate';
		let formData = new FormData();
		this.http.post(this.apiurl.end_point+`/${api}`, formData)
	    .subscribe((response:any) => {
	         if(response){
	         	console.log(response,'response');
	         	this.page_data = response;
				 this.userForm.controls.content_one.setValue(response.traveldesign.content_one);
				 this.userForm.controls.sharkpads_way_text.setValue(response.traveldesign.sharkpads_way_text);
				 this.userForm.controls.personal_portfolio_text.setValue(response.traveldesign.personal_portfolio_text);
				 this.userForm.controls.technology_text.setValue(response.traveldesign.technology_text);
				 this.userForm.controls.community_relations_text.setValue(response.traveldesign.community_relations_text);
				 this.userForm.controls.identification_text.setValue(response.traveldesign.identification_text);
				 this.userForm.controls.contract_and_negotitations_text.setValue(response.traveldesign.contract_and_negotitations_text);
				 this.userForm.controls.industry_insight_text.setValue(response.traveldesign.industry_insight_text);
				 this.userForm.controls.partner_text.setValue(response.traveldesign.partner_text);
				 this.userForm.controls.security_text.setValue(response.traveldesign.security_text);
				 this.userForm.controls.revenue_management_reporting_text.setValue(response.traveldesign.revenue_management_reporting_text);
				 this.userForm.controls.marketing_and_branding_text.setValue(response.traveldesign.marketing_and_branding_text);
				 this.userForm.controls.page_title.setValue(response.traveldesign.page_title);
				 
	     	 }
	    });


		this.http.get<any>('https://api.npms.io/v2/search?q=scope:angular').subscribe(data => {
        	//this.totalAngularPackages = data.total;
    	})   

		this.loading$ = this.store.pipe(select(selectUsersActionLoading));
		const routeSubscription =  this.activatedRoute.params.subscribe(params => {
			const id = params.id;
			if (id && id > 0) {
				this.store.pipe(select(selectUserById(id))).subscribe(res => {
					if (res) {
						this.user = res;
						this.rolesSubject.next(this.user.roles);
						this.addressSubject.next(this.user.address);
						this.soicialNetworksSubject.next(this.user.socialNetworks);
						this.oldUser = Object.assign({}, this.user);
						this.initUser();
					}
				});
			} else {
				this.user = new User();
				this.user.clear();
				this.rolesSubject.next(this.user.roles);
				this.addressSubject.next(this.user.address);
				this.soicialNetworksSubject.next(this.user.socialNetworks);
				this.oldUser = Object.assign({}, this.user);
				this.initUser();
			}
		});
		this.subscriptions.push(routeSubscription);
	}

	ngOnDestroy() {
		this.subscriptions.forEach(sb => sb.unsubscribe());
	}

	/**
	 * Init user
	 */
	initUser() {
		this.createForm();
		if (!this.user.id) {
			this.subheaderService.setTitle('Create user');
			this.subheaderService.setBreadcrumbs([
				{ title: 'User Management', page: `user-management` },
				{ title: 'Users',  page: `user-management/users` },
				{ title: 'Create user', page: `user-management/users/add` }
			]);
			return;
		}
		this.subheaderService.setTitle('Edit user');
		this.subheaderService.setBreadcrumbs([
			{ title: 'User Management', page: `user-management` },
			{ title: 'Users',  page: `user-management/users` },
			{ title: 'Edit user', page: `user-management/users/edit`, queryParams: { id: this.user.id } }
		]);
	}

	/**
	 * Create form
	 */
	createForm() {
		this.userForm = this.userFB.group({
			content_one: [this.user.content_one, ""],
			sharkpads_way_text: [this.user.sharkpads_way_text, ""],
			personal_portfolio_text: [this.user.personal_portfolio_text, ""],
			technology_text: [this.user.technology_text, ""],
			community_relations_text: [this.user.community_relations_text, ""],
			identification_text: [this.user.identification_text, ""],
			contract_and_negotitations_text: [this.user.contract_and_negotitations_text, ""],
			industry_insight_text: [this.user.industry_insight_text, ""],
			partner_text: [this.user.partner_text, ""],
			security_text: [this.user.security_text, ""],
			revenue_management_reporting_text: [this.user.revenue_management_reporting_text, ""],
			marketing_and_branding_text: [this.user.marketing_and_branding_text, ""],
			page_title: [this.user.page_title, ""]
		});
	}

	/**
	 * Redirect to list
	 *
	 */
	goBackWithId() {
		const url = `/user-management/users`;
		this.router.navigateByUrl(url, { relativeTo: this.activatedRoute });
	}

	/**
	 * Refresh user
	 *
	 * @param isNew: boolean
	 * @param id: number
	 */
	refreshUser(isNew: boolean = false, id = 0) {
		let url = this.router.url;
		if (!isNew) {
			this.router.navigate([url], { relativeTo: this.activatedRoute });
			return;
		}

		url = `/user-management/users/edit/${id}`;
		this.router.navigateByUrl(url, { relativeTo: this.activatedRoute });
	}

	/**
	 * Reset
	 */
	reset() {
		this.user = Object.assign({}, this.oldUser);
		this.createForm();
		this.hasFormErrors = false;
		this.userForm.markAsPristine();
  		this.userForm.markAsUntouched();
  		this.userForm.updateValueAndValidity();
	}

	/**
	 * Save data
	 *
	 * @param withBack: boolean
	 */
	onSumbit(withBack: boolean = false) {
		this.hasFormErrors = false;
		const controls = this.userForm.controls;

		console.log(controls,'test');
		
		if (this.userForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);

			this.hasFormErrors = true;
			this.selectedTab = 0;
			return;
		}

		let formData = new FormData();
		//let formData = new FormData();
		if(this.uploadedFiles){
		    for (var i = 0; i < this.uploadedFiles.length; i++) {
		        formData.append("uploads", this.uploadedFiles[i], this.uploadedFiles[i].name);
		    }
		}

		if(this.pic_one){
		    for (var i = 0; i < this.pic_one.length; i++) {
		        formData.append("pic_one", this.pic_one[i], this.pic_one[i].name);
		    }
		}

		if(this.pic_two){
		    for (var i = 0; i < this.pic_two.length; i++) {
		        formData.append("pic_two", this.pic_two[i], this.pic_two[i].name);
		    }
		}

		if(this.pic_three){
		    for (var i = 0; i < this.pic_three.length; i++) {
		        formData.append("pic_three", this.pic_three[i], this.pic_three[i].name);
		    }
		}

		if(this.personal_portfolio){
		    for (var i = 0; i < this.personal_portfolio.length; i++) {
		        formData.append("personal_portfolio", this.personal_portfolio[i], this.personal_portfolio[i].name);
		    }
		}

		if(this.technology){
		    for (var i = 0; i < this.technology.length; i++) {
		        formData.append("technology", this.technology[i], this.technology[i].name);
		    }
		}

		if(this.community_relations){
		    for (var i = 0; i < this.community_relations.length; i++) {
		        formData.append("community_relations", this.community_relations[i], this.community_relations[i].name);
		    }
		}

		
		if(this.identification){
		    for (var i = 0; i < this.identification.length; i++) {
		        formData.append("identification", this.identification[i], this.identification[i].name);
		    }
		}

		if(this.contract_and_negotitations){
		    for (var i = 0; i < this.contract_and_negotitations.length; i++) {
		        formData.append("contract_and_negotitations", this.contract_and_negotitations[i], this.contract_and_negotitations[i].name);
		    }
		}

		if(this.industry_insight){
		    for (var i = 0; i < this.industry_insight.length; i++) {
		        formData.append("industry_insight", this.industry_insight[i], this.industry_insight[i].name);
		    }
		}

		if(this.partner){
		    for (var i = 0; i < this.partner.length; i++) {
		        formData.append("partner", this.partner[i], this.partner[i].name);
		    }
		}

		if(this.security){
		    for (var i = 0; i < this.security.length; i++) {
		        formData.append("security", this.security[i], this.security[i].name);
		    }
		}

		if(this.revenue_management_reporting){
		    for (var i = 0; i < this.revenue_management_reporting.length; i++) {
		        formData.append("revenue_management_reporting", this.revenue_management_reporting[i], this.revenue_management_reporting[i].name);
		    }
		}

		if(this.marketing_and_branding){
		    for (var i = 0; i < this.marketing_and_branding.length; i++) {
		        formData.append("marketing_and_branding", this.marketing_and_branding[i], this.marketing_and_branding[i].name);
		    }
		}


		if(this.sharkpads_way){
		    for (var i = 0; i < this.sharkpads_way.length; i++) {
		        formData.append("sharkpads_way", this.sharkpads_way[i], this.sharkpads_way[i].name);
		    }
		}

		

	    formData.append("page_id", this.getparam('special'));
		formData.append("content_one",controls.content_one.value);
		formData.append("page_title",controls.page_title.value);
		
		formData.append("sharkpads_way_text",controls.sharkpads_way_text.value);
		formData.append("personal_portfolio_text",controls.personal_portfolio_text.value);
		formData.append("technology_text",controls.technology_text.value);
		formData.append("community_relations_text",controls.community_relations_text.value);
		formData.append("identification_text",controls.identification_text.value);
		formData.append("contract_and_negotitations_text",controls.contract_and_negotitations_text.value);
		formData.append("industry_insight_text",controls.industry_insight_text.value);
		formData.append("partner_text",controls.partner_text.value);
		formData.append("security_text",controls.security_text.value);
		formData.append("revenue_management_reporting_text",controls.revenue_management_reporting_text.value);
		formData.append("marketing_and_branding_text",controls.marketing_and_branding_text.value);
	    console.log(formData,'formData')

	    this.message = 'Uploading file';
	    let api = 'api_real_estate';
	    this.http.post(this.apiurl.end_point+`/${api}`, formData)
	    .subscribe((response) => {
	         if(response){
	         	this.message = 'Page updated successfully';
	         	alert(this.message);
	     	 }
	    });

	    this.message = ' ';
	}

	/**
	 * Returns prepared data for save
	 */
	prepareUser(): User {
		const controls = this.userForm.controls;
		const _user = new User();
		_user.clear();
		_user.roles = this.rolesSubject.value;
		_user.address = this.addressSubject.value;
		_user.socialNetworks = this.soicialNetworksSubject.value;
		_user.accessToken = this.user.accessToken;
		_user.refreshToken = this.user.refreshToken;
		_user.pic = this.user.pic;
		_user.id = this.user.id;
		_user.username = controls.username.value;
		_user.email = controls.email.value;
		_user.fullname = controls.fullname.value;
		_user.occupation = controls.occupation.value;
		_user.phone = controls.phone.value;
		_user.companyName = controls.companyName.value;
		_user.password = this.user.password;
		return _user;
	}

	/**
	 * Add User
	 *
	 * @param _user: User
	 * @param withBack: boolean
	 */
	addUser(_user: User, withBack: boolean = false) {
		this.store.dispatch(new UserOnServerCreated({ user: _user }));
		const addSubscription = this.store.pipe(select(selectLastCreatedUserId)).subscribe(newId => {
			const message = `New user successfully has been added.`;
			this.layoutUtilsService.showActionNotification(message, MessageType.Create, 5000, true, true);
			if (newId) {
				if (withBack) {
					this.goBackWithId();
				} else {
					this.refreshUser(true, newId);
				}
			}
		});
		this.subscriptions.push(addSubscription);
	}

	/**
	 * Update user
	 *
	 * @param _user: User
	 * @param withBack: boolean
	 */
	updateUser(_user: User, withBack: boolean = false) {
		// Update User
		// tslint:disable-next-line:prefer-const

		const updatedUser: Update<User> = {
			id: _user.id,
			changes: _user
		};
		this.store.dispatch(new UserUpdated( { partialUser: updatedUser, user: _user }));
		const message = `User successfully has been saved.`;
		this.layoutUtilsService.showActionNotification(message, MessageType.Update, 5000, true, true);
		if (withBack) {
			this.goBackWithId();
		} else {
			this.refreshUser(false);
		}
	}

	/**
	 * Returns component title
	 */
	getComponentTitle() {
		let result = 'Real Estate Content';
		if (!this.user || !this.user.id) {
			return result;
		}

		result = `Edit user - ${this.user.fullname}`;
		return result;
	}

	/**
	 * Close Alert
	 *
	 * @param $event: Event
	 */
	onAlertClose($event) {
		this.hasFormErrors = false;
	}
}
