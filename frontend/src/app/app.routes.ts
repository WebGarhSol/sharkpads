import { Routes } from '@angular/router';

import { AboutComponent } from './about/about.component';
import { AboutItemComponent } from './about/about-item/about-item.component';
import { AboutHomeComponent } from './about/about-home/about-home.component';
import { HomeComponent } from './home/home.component';
import { TravelDesignComponent } from './traveldesign/traveldesign.component';
import { RentalsComponent } from './rentals/rentals.component';
import { RealEstateComponent } from './real_estate/real_estate.component';
import { LifeStyleComponent } from './lifestyle/lifestyle.component';
import { InquireComponent } from './inquire/inquire.component';
import { PropertyComponent } from './property_details/property.component';
import { BookingComponent } from './booking/booking.component';

export const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'experiences', component: TravelDesignComponent },
  { path: 'rentals', component: RentalsComponent },
  { path: 'invest', component: RealEstateComponent },
  { path: 'life_style', component: LifeStyleComponent },
  { path: 'inquire', component: InquireComponent },
  { path: 'property/:id/:from/:to/:guests', component: PropertyComponent },
  { path: 'booking/:id/:from/:to/:guests', component: BookingComponent },
  {
    path: 'about',
    component: AboutComponent,
    children: [
      { path: '', component: AboutHomeComponent },
      { path: 'item/:id', component: AboutItemComponent }
    ]
  }
];