import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import {APP_BASE_HREF} from '@angular/common';
import { FormGroup, ReactiveFormsModule, FormsModule, FormBuilder } from '@angular/forms';
import { CommonModule } from '@angular/common';  
import { HashLocationStrategy, LocationStrategy } from '@angular/common';

import { routes } from './app.routes';
import { AppComponent } from './app.component';
import { AboutComponent } from './about/about.component';

import { HomeComponent } from './home/home.component';
import { TravelDesignComponent } from './traveldesign/traveldesign.component';
import { InquireComponent } from './inquire/inquire.component';
import { LifeStyleComponent } from './lifestyle/lifestyle.component';
import { RealEstateComponent } from './real_estate/real_estate.component';
import { RentalsComponent } from './rentals/rentals.component';
import { PropertyComponent } from './property_details/property.component';
import { BookingComponent } from './booking/booking.component';

import { AboutItemComponent } from './about/about-item/about-item.component';
import { AboutHomeComponent } from './about/about-home/about-home.component';
import { HttpClientModule } from '@angular/common/http';
import { SafePipe } from './pipes/safe.pipe';
import { GoogleMapsModule } from '@angular/google-maps'
import { NgwWowModule } from 'ngx-wow';
import { Daterangepicker } from 'ng2-daterangepicker';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
<<<<<<< HEAD
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
=======
>>>>>>> 8984940b6fd992eef6756565fbc19ce2057b00d1

@NgModule({
  providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}],
  imports: [
    BrowserModule,
    FormsModule, 
    ReactiveFormsModule,
    CommonModule,
    HttpClientModule,
    GoogleMapsModule,
    NgwWowModule,
    Daterangepicker,
<<<<<<< HEAD
    NgbModule,
    BrowserAnimationsModule,
=======
>>>>>>> 8984940b6fd992eef6756565fbc19ce2057b00d1
    RouterModule.forRoot(routes, {
      scrollPositionRestoration: 'enabled'
    })
  ],
  declarations: [AppComponent, AboutComponent,TravelDesignComponent,InquireComponent,LifeStyleComponent,RealEstateComponent,RentalsComponent, HomeComponent, AboutItemComponent, AboutHomeComponent, SafePipe, PropertyComponent, BookingComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
