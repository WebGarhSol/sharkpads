import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ApiService } from './../services/api.service';
import * as $ from 'jquery';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './booking.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./booking.component.css']
})
export class BookingComponent implements OnInit {
  private sub: any;
  currentYear: number=new Date().getFullYear();
  page_setting: any;
  page_data: any;
  slides:any;
  guesty_api_response: any;
  guesty_api_quote:any;
  registerForm: FormGroup;
  calculate_date_diff: any;
  member: any;
  submitted = false;
  params_param: any;
  wait:any;
  menus: any;

  constructor(public router: Router, private formBuilder: FormBuilder, private http: HttpClient,public apiurl: ApiService,private route: ActivatedRoute) {
  	let api = 'get_settings';
    let formData = new FormData();

		this.http.post(this.apiurl.end_point+`/${api}`, formData)
	    .subscribe((response) => {
	         if(response){
             this.page_setting = response;
             console.log(response,'response')
	     	  }
    });

    api = 'get_slider';
    var _that = this;
    
    this.http.get(this.apiurl.end_point+`/${api}`)
    .subscribe((response:any) => {
        if(response){
          this.slides = response.pages;
        }
    });

    api = 'get_pages';
    this.http.get(this.apiurl.end_point+`/${api}`)
    .subscribe((response:any) => {
        if(response){
          this.menus = response.pages;
        }
    });
  }

  ngOnInit() {
    let api = 'get_rentals';
		let formData = new FormData();
		this.http.post(this.apiurl.end_point+`/${api}`, formData)
	    .subscribe((response) => {
	        if(response){
            this.page_data = response;
	     	  }
    });

    this.sub = this.route.params.subscribe(params => {
      console.log('params',params);
      this.params_param = params;

      this.member = params.guests;
      if(params.guests=='Guests' || params.guests=='guests'){
         this.member = 1;
      }

      let oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
      let firstDate = new Date(params.from);
      let secondDate = new Date(params.to);
      this.calculate_date_diff = Math.round(Math.abs(((+firstDate) - (+secondDate)) / oneDay));

      this.registerForm = this.formBuilder.group({
        firstname: ['',Validators.required],
        lastname: ['',Validators.required],
        email: ['',Validators.required],
        phone: ['',Validators.required],
        privacy: ['',Validators.required],
        interested: ['',Validators.required],
        message: ['',Validators.required]
      });

      let api = 'api_property';
      let formData = new FormData();
      formData.append("id",  params.id);
      formData.append("from",  params.from);
      formData.append("to",  params.to);
      formData.append("guests",  params.guests);

      this.http.post(this.apiurl.end_point+`/${api}`, formData)
	    .subscribe((response:any) => {
	        if(response){
            let res = JSON.parse(JSON.stringify(response));
            this.guesty_api_response = res.response;
            console.log(this.guesty_api_response,'res single object');
	     	  }
      });

      api = 'api_property_quote';
      this.http.post(this.apiurl.end_point+`/${api}`, formData)
	    .subscribe((response:any) => {
	        if(response){
            let res = JSON.parse(JSON.stringify(response));
            this.guesty_api_quote = res.response;
            console.log(res,'res quote');
            //this.guesty_api_response = res.response;
            //console.log(this.guesty_api_response,'res single object');
	     	  }
      });
    });
  }

  search_data(){
      let formData = new FormData();
      const controls = this.registerForm.controls;
      let api = 'api_reservations';

      formData.append("id",controls.id.value);
      formData.append("from",controls.from.value);
      formData.append("to",controls.from.value);
      formData.append("guests",controls.guests.value);
     

      this.http.post(this.apiurl.end_point+`/${api}`, formData)
	    .subscribe((response:any) => {
	        if(response){
            let res = JSON.parse(JSON.stringify(response));
            if(res.status){
              this.guesty_api_response = res.response;
              console.log(this.guesty_api_response,'res single object');
              alert('Booked successfully');
            }else{
              alert(res.response);
            }
	     	  }
      });
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    // console.log(this.registerForm.FormGroup.controls.name.errors.required,'test');
    //alert('sssssss')
    console.log(this.registerForm,'ddddddddddddd')
    if (this.registerForm.invalid) {
      return;
    }
    
    this.wait = 'Please wait...';

    let controls = this.registerForm.controls;
    let formData = new FormData();
    let api = 'api_reservations';

    //formData.append("controls",controls);
    formData.append("from",this.params_param.from);
    formData.append("to",this.params_param.to);
    formData.append("guests",this.params_param.guests);
    formData.append("id",this.params_param.id);
    formData.append("calculate_date_diff",this.calculate_date_diff);
    formData.append("firstname",controls.firstname.value);
    formData.append("lastname",controls.lastname.value);
    formData.append("email",controls.email.value);
    formData.append("phone",controls.phone.value);
    formData.append("message",controls.message.value);
    formData.append("privacy",controls.privacy.value);
    formData.append("interested",controls.interested.value);
    formData.append("price",this.guesty_api_quote.money.subTotalPrice);

    

    this.http.post(this.apiurl.end_point+`/${api}`, formData)
    .subscribe((response:any) => {
        if(response){
          let res = JSON.parse(JSON.stringify(response));
          if(res.status){
            this.wait = '';
            //this.guesty_api_response = res.response;
            //console.log(this.guesty_api_response,'res single object');
            alert('Booked successfully');
            this.submitted = false;
            this.registerForm.reset();
          }else{
            alert(res.response);
          }
        }
    });
  }

  onSearchChange(searchValue: string): void {  
    console.log(searchValue);
  }

  openNav() {
    document.getElementById("mySidenav").style.width = "250px";
  }

  closeNav() {
    document.getElementById("mySidenav").style.width = "0";
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}