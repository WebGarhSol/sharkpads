import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ApiService } from './../services/api.service';
import * as $ from 'jquery';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './property.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./property.component.css']
})
export class PropertyComponent implements OnInit {
  private sub: any;
  currentYear: number=new Date().getFullYear();
  page_setting: any;
  page_data: any;
  slides:any;
  guesty_api_response: any;
  guesty_api_quote:any;
  registerForm: FormGroup;
  calculate_date_diff: any;
  menus: any;

  constructor(public router: Router, private formBuilder: FormBuilder, private http: HttpClient,public apiurl: ApiService,private route: ActivatedRoute) {
  	let api = 'get_settings';
    let formData = new FormData();

		this.http.post(this.apiurl.end_point+`/${api}`, formData)
	    .subscribe((response) => {
	         if(response){
             this.page_setting = response;
             console.log(response,'response')
	     	  }
    });

    api = 'get_slider';
    var _that = this;
    
    this.http.get(this.apiurl.end_point+`/${api}`)
    .subscribe((response:any) => {
        if(response){
          this.slides = response.pages;
        }
    });

    api = 'get_pages';
    this.http.get(this.apiurl.end_point+`/${api}`)
    .subscribe((response:any) => {
        if(response){
          this.menus = response.pages;
        }
    });
  }

  ngOnInit() {
    let api = 'get_rentals';
		let formData = new FormData();
		this.http.post(this.apiurl.end_point+`/${api}`, formData)
	    .subscribe((response) => {
	        if(response){
            this.page_data = response;
	     	  }
    });

    this.sub = this.route.params.subscribe(params => {
      console.log('params',params);

      let member = params.guests;
      if(params.guests=='Guests' || params.guests=='guests'){
         member = 1;
      }

      this.registerForm = this.formBuilder.group({
        from: [params.from,[]],
        to: [params.to,[]],
        guests: [member,[]],
        id: [params.id,[]],
      });

      const oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
      const firstDate = new Date(params.from);
      const secondDate = new Date(params.to);

      this.calculate_date_diff = Math.round(Math.abs(((+firstDate) - (+secondDate)) / oneDay));

      let api = 'api_property';
      let formData = new FormData();
      formData.append("id",  params.id);
      formData.append("from",  params.from);
      formData.append("to",  params.to);
      formData.append("guests",  params.guests);

      this.http.post(this.apiurl.end_point+`/${api}`, formData)
	    .subscribe((response:any) => {
	        if(response){
            let res = JSON.parse(JSON.stringify(response));
            this.guesty_api_response = res.response;
            console.log(this.guesty_api_response,'res single object');
	     	  }
      });

      api = 'api_property_quote';
      this.http.post(this.apiurl.end_point+`/${api}`, formData)
	    .subscribe((response:any) => {
	        if(response){
            let res = JSON.parse(JSON.stringify(response));
            this.guesty_api_quote = res.response;
            console.log(res,'res quote');
            //this.guesty_api_response = res.response;
            //console.log(this.guesty_api_response,'res single object');
	     	  }
      });
    });
  }

  search_data(){
      const controls = this.registerForm.controls;
      this.router.navigate(['booking', controls.id.value, controls.from.value ,controls.to.value, controls.guests.value]);

      return;
      
      let api = 'api_reservations';
      let formData = new FormData();
      formData.append("id",controls.id.value);
      formData.append("from",controls.from.value);
      formData.append("to",controls.from.value);
      formData.append("guests",controls.guests.value);

      this.http.post(this.apiurl.end_point+`/${api}`, formData)
	    .subscribe((response:any) => {
	        if(response){
            let res = JSON.parse(JSON.stringify(response));
            if(res.status){
              this.guesty_api_response = res.response;
              console.log(this.guesty_api_response,'res single object');
              alert('Booked successfully');
            }else{
              alert(res.response);
            }
	     	  }
      });
  }

  onSearchChange(searchValue: string): void {  
    console.log(searchValue);
  }

  openNav() {
    document.getElementById("mySidenav").style.width = "250px";
  }

  closeNav() {
    document.getElementById("mySidenav").style.width = "0";
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}