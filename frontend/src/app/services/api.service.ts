import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  
  
  time = 2000;
  
  //end_point = "http://localhost:3000";
  //end_point = "https://apptest.webgarh.net:3000";
  //end_point = "https://thesharkpad.com:3000";
  end_point = "https://sharkpads.com:3000";
  
  constructor() {

  }
}
