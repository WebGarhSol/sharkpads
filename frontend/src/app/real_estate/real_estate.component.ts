import { Component, OnInit, ViewEncapsulation,Inject } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ApiService } from './../services/api.service';
import * as $ from 'jquery';
import { NgwWowService } from 'ngx-wow';
<<<<<<< HEAD
import {NgbModal, ModalDismissReasons, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { DOCUMENT } from '@angular/common';
=======
>>>>>>> 8984940b6fd992eef6756565fbc19ce2057b00d1

@Component({
  selector: 'app-home',
  templateUrl: './real_estate.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./real_estate.component.css']
})
export class RealEstateComponent implements OnInit {
  ImagePath: string; 
  currentYear: number=new Date().getFullYear();
  end_point = "";
  //end_point = "http://139.59.91.66:3000"
  page_data: any;
  page_setting: any;
  slider_html: any;
  slides:any;
  loading_img = true;
<<<<<<< HEAD
  words : any;
  menus: any;
  mission_statement = 'We guide travelers and investors to their lifestyle destinations via luxury real estate. We always put service first and profits second. And we will always take an innovative approach to problem solving.';
  full_mission_statement = 'We guide travelers and investors to their lifestyle destinations via luxury real estate. We always put service first and profits second. And we will always take an innovative approach to problem solving.';
  full_mission_statement_mode = false;
  sharkpad_way = false;
  industry_insight = false;
  Identification = false;
  negotiations = false;
  community = false;

  security = false;
  technology = false;
  branding = false;
  revenue = false;
  portfolio = false;
  partners = false;
  closeResult: string;
  hidden = false;
  current_menu_visible = 0;
  current_menu_visible2 = 0;

  constructor(@Inject(DOCUMENT) private document: Document,private modalService: NgbModal,public router: Router,private http: HttpClient,public apiurl: ApiService, private wowService: NgwWowService) {
    var $animation_elements = $('.animation-element');
    var $window = $(window);
    let id = localStorage.getItem("redirect_page");

    //alert(id)
    if(id!="undefined" || id){
      setTimeout(function(){
        var offSet = 70;
        var obj = $('#' + id);
        if(obj.length){
          var offs = obj.offset();
          var targetOffset = offs.top - offSet;
          $('html,body').animate({ scrollTop: targetOffset }, 0);
        }

        localStorage.setItem("redirect_page","");
      },4000)
      
    }

    function check_if_in_view() {
      var window_height = $window.height();
      console.log('dddddddddddd')
      var window_top_position = $window.scrollTop();
      var window_bottom_position = (window_top_position + window_height);
    
      $.each($animation_elements, function() {
        var $element = $(this);
        var element_height = $element.outerHeight();
        var element_top_position = $element.offset().top;
        var element_bottom_position = (element_top_position + element_height);
    
        //check to see if this current container is within viewport
        if ((element_bottom_position >= window_top_position) &&
            (element_top_position <= window_bottom_position)) {
          $element.addClass('in-view');
        } else {
          $element.removeClass('in-view');
        }
      });
    }

    $window.on('scroll resize', check_if_in_view);
    $window.trigger('scroll');
    
    
    let api = 'get_real_estate';
    let formData = new FormData();
    setTimeout(() => this.loading_img = false, 4000);
    $(document).on('scroll', function() {
      if ($(this).scrollTop() >= $('.make_over').position().top && !$('.make_over_animate').hasClass('done')) {
        console.log('I have been reached');
        $('.make_over_animate').fadeOut(100).fadeIn(2000);
        $('.make_over_animate').addClass('done')
      }
    }) 

    /*
    var container = $(".sub-menu");
    $(document).mouseup(function(e) {
        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0){
            container.hide();
            $('.sub-menu').attr("style", "display: none !important");
            $('.icon-sub-menu').removeClass('up-icon');
        }
    });
    */

=======
  menus: any;

  constructor(public router: Router,private http: HttpClient,public apiurl: ApiService, private wowService: NgwWowService) {
  	let api = 'get_real_estate';
    let formData = new FormData();
    setTimeout(() => this.loading_img = false, this.apiurl.time);

>>>>>>> 8984940b6fd992eef6756565fbc19ce2057b00d1
		this.http.post(this.apiurl.end_point+`/${api}`, formData)
	    .subscribe((response) => {
	         if(response){
             this.page_data = response;
             console.log(response,'response');
             this.wowService.init();
<<<<<<< HEAD

            setTimeout(function(){
             $('.eprince-inner').each(function(){           
              let img_src = $(this).find('img.only_desktop_show').attr('src');
              $(this).find('.dfd-heading-delimiter').after($('<img>',{hidden:true,class:'only_mobile_show',src:img_src}))
             });
            },3000)
=======
>>>>>>> 8984940b6fd992eef6756565fbc19ce2057b00d1
	     	  }
    });

    api = 'get_slider';
    var _that = this;
    
    this.http.get(this.apiurl.end_point+`/${api}`)
    .subscribe((response:any) => {
        if(response){
          _that.slider_html = '';
          this.slides = response.pages;
          
          this.wowService.init();

          response.pages.forEach(function(element) {
            //console.log(element,'element')
            _that.slider_html += '<div>';
            _that.slider_html += '<img src="assets/page/'+element.image+'" alt="">';
            _that.slider_html += '<div class="content-wrap"><div class="info-box-title feature-title" style="">'+element.caption+'</div></div>';
            _that.slider_html += '</div>';
          });
        }
    });

    api = 'get_pages';
    this.http.get(this.apiurl.end_point+`/${api}`)
    .subscribe((response:any) => {
        if(response){
          this.menus = response.pages;
        }
    });
  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  redirect_to_url(url){
    //$(document).find('#back-to-top').click();

    
    //$('.d-block').remove();
    //$('body').removeClass('modal-open');
    //$('body').css('padding-right', '0px');
    //$('.d-block').css('overflow-y', 'auto');
    //alert(url)
    this.router.navigateByUrl(url); 
    setTimeout(function(){
      this.document.location.reload();
    },100);   
    //this.getDismissReason(1);
  }

  goToUrl(url): void {
    window.open(url);
  }

  scrollToID(id, speed) {
    var offSet = 70;
    var obj = $('#' + id);
    if(obj.length){
      var offs = obj.offset();
      var targetOffset = offs.top - offSet;
      $('html,body').animate({ scrollTop: targetOffset }, speed);
    }

    
    $('#mySidenav').css('width','0px');
     this.router.navigate(['/invest']);
    //$('.sub-menu').attr("style", "display: none !important");
  }

  shorten(text,max) {
    return text.split(" ").splice(0,max).join(" ");
  }

  more_info(str){
    if(str=='mission_statement'){
        this.full_mission_statement_mode = true;
    }

    if(str=='sharkpad_way'){
        this.sharkpad_way = true;
    }

    if(str=='industry_insight'){
       this.industry_insight = true;
    }

    if(str=='Identification'){
      this.Identification = true;
    }

    if(str=='negotiations'){
      this.negotiations = true;
    }

    if(str=='community'){
      this.community = true;
    }

    if(str=='security'){
      this.security = true;
    }

    if(str=='technology'){
      this.technology = true;
    }

    if(str=='branding'){
      this.branding = true;
    }

    if(str=='revenue'){
      this.revenue = true;
    }

    if(str=='portfolio'){
      this.portfolio = true;
    }

    if(str=='partners'){
      this.partners = true;
    }
  }

  CheckLen(str){ 
  
    // c counts the number of words of input value 
    this.words=str.split(' ').length;  
  
    // We have set that minimum word count should 
    // be 5 or more and the maximum should be 20. 
    if(this.words>20){         
      return true;
    } 
    if(this.words<20){ 
      return false;
    } 
  } 

  ngOnInit() {
    let api = 'get_settings';
    let formData = new FormData();
    this.http.post(this.apiurl.end_point+`/${api}`,formData)
      .subscribe((response) => {
          
          this.wowService.init();
          
          if(response){
            this.page_setting = response;
            console.log(response,'response')
          }
    });
  }

  menu_navigate(menu){
    
    //$('html, body').animate({scrollTop:0}, '300');
    this.router.navigate(['/'+menu]);
  }

  openNav() {
    document.getElementById("mySidenav").style.width = "250px";
  }

  closeNav() {
    document.getElementById("mySidenav").style.width = "0";
  }

  externalLink(e,what){
    e.preventDefault();
    window.open(what, '_blank');
  }  

  displayallmenu(e,what,menu_class){
    e.preventDefault();
    console.log($(this),'test');
    if(what==1 && this.current_menu_visible==0){
      $('.'+menu_class).attr("style", "display: block !important");
      $('.'+menu_class).prev().addClass('up-icon');
      this.current_menu_visible = 1;
    }else{
      $('.'+menu_class).attr("style", "display: none !important");
      $('.'+menu_class).prev().removeClass('up-icon');
      this.current_menu_visible = 0;
    }
  }

  /*
  displayallmenu2(e,what){
    e.preventDefault();
    if(what==1 && this.current_menu_visible2==0){
      $('.life_style_menu').attr("style", "display: block !important");
      $('.icon-sub-menu-life-style').addClass('up-icon');
      this.current_menu_visible2 = 1;
    }else{
      $('.life_style_menu').attr("style", "display: none !important");
      $('.icon-sub-menu-life-style').removeClass('up-icon');
      this.current_menu_visible2 = 0;
    }
  }

  */
}