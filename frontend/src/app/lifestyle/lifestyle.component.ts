import { Component, OnInit, ViewEncapsulation,Inject } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ApiService } from './../services/api.service';
import * as $ from 'jquery';
import { NgwWowService } from 'ngx-wow';
<<<<<<< HEAD
import {NgbModal, ModalDismissReasons, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { DOCUMENT } from '@angular/common';
=======
>>>>>>> 8984940b6fd992eef6756565fbc19ce2057b00d1

@Component({
  selector: 'app-home',
  templateUrl: './lifestyle.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./lifestyle.component.css']
})
export class LifeStyleComponent implements OnInit {
  ImagePath: string; 
  currentYear: number=new Date().getFullYear();
  end_point = "http://localhost:3000";
  //end_point = "http://139.59.91.66:3000"
  page_data: any;
  page_setting: any;
  slides:any;
  slider_html: any;
  loading_img = true;
  menus: any;
<<<<<<< HEAD
  current_menu_visible = 0;
  current_menu_visible2 = 0;
  hospitality = false;
  at_shark = false;
  candle = false;
  closeResult: string;
  
  constructor(@Inject(DOCUMENT) private document: Document,private modalService: NgbModal,public router: Router,private http: HttpClient,public apiurl: ApiService, private wowService: NgwWowService) {
=======

  constructor(public router: Router,private http: HttpClient,public apiurl: ApiService, private wowService: NgwWowService) {
>>>>>>> 8984940b6fd992eef6756565fbc19ce2057b00d1
  	let api = 'get_lifestyle';
    let formData = new FormData();
    setTimeout(() => this.loading_img = false, this.apiurl.time);

    this.attachscript();  
		this.http.post(this.apiurl.end_point+`/${api}`, formData)
	    .subscribe((response) => {
	         if(response){
             this.page_data = response;
             console.log(response,'response');
             this.wowService.init();
<<<<<<< HEAD

             setTimeout(function(){
              $('.eprince-inner').each(function(){           
                let img_src = $(this).find('img.only_desktop_show').attr('src');
                $(this).find('.dfd-heading-delimiter').after($('<img>',{hidden:true,class:'only_mobile_show',src:img_src}))
              });
            },3000);
=======
>>>>>>> 8984940b6fd992eef6756565fbc19ce2057b00d1
	     	  }
    });

    api = 'get_slider';
    var _that = this;
    
    this.http.get(this.apiurl.end_point+`/${api}`)
    .subscribe((response:any) => {
        
        this.wowService.init();
      
        if(response){
          _that.slider_html = '';
          this.slides = response.pages;
            
          response.pages.forEach(function(element) {
            //console.log(element,'element')
            _that.slider_html += '<div class="redirect '+element.link+'">';
            _that.slider_html += '<img src="assets/page/'+element.image+'" alt="">';
            _that.slider_html += '<div class="content-wrap"><div class="info-box-title feature-title" style="">'+element.caption+'</div></div>';
            _that.slider_html += '</div>';
          });
        }
    });

    api = 'get_pages';
    this.http.get(this.apiurl.end_point+`/${api}`)
    .subscribe((response:any) => {
        if(response){
          this.menus = response.pages;
        }
    });
  }


  attachscript(){
    $('script').each(function() {
      if(this.src.indexOf('pinterest') != -1){
          $(this).remove();
      }
    });
    
    let mainUrl = "https://assets.pinterest.com/js/pinit_main.js"+"?"+Math.random();
    var head = document.getElementsByTagName('head')[0];
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = mainUrl;
    head.appendChild(script);

    setTimeout(function(){
      $('span').each(function(){
        let cls = $(this).attr('class');
        if(cls && cls.indexOf('embed_grid') != -1){
          $( this ).width('1018px');
          $( this ).addClass('set_width');
        }

        if(cls && cls.indexOf('bd') != -1){
          $( this ).height('530px');
          $( this ).addClass('set_height');
        }
      });  
    },4000);
}

  ngOnInit() {
    let api = 'get_settings';
    let formData = new FormData();
    this.http.post(this.apiurl.end_point+`/${api}`, formData)
      .subscribe((response) => {
          
          this.wowService.init();

          if(response){
            this.page_setting = response;
            console.log(response,'response')
          }
    });
  }

  openNav() {
    document.getElementById("mySidenav").style.width = "250px";
  }

  closeNav() {
    document.getElementById("mySidenav").style.width = "0";
  }
  
  menu_navigate(menu){   
   // $('html, body').animate({scrollTop:0}, '300');
    this.router.navigate(['/'+menu]);
  }

  externalLink(e,what){
    e.preventDefault();
    window.open(what, '_blank');
  } 

  displayallmenu(e,what,menu_class){
    e.preventDefault();
    console.log($(this),'test');
    if(what==1 && this.current_menu_visible==0){
      $('.'+menu_class).attr("style", "display: block !important");
      $('.'+menu_class).prev().addClass('up-icon');
      this.current_menu_visible = 1;
    }else{
      $('.'+menu_class).attr("style", "display: none !important");
      $('.'+menu_class).prev().removeClass('up-icon');
      this.current_menu_visible = 0;
    }
  } 

  /*

  displayallmenu2(e,what){
    e.preventDefault();

    if(what==1 && this.current_menu_visible2==0){
      $('.life_style_menu').attr("style", "display: block !important");
      $('.icon-sub-menu-life-style').addClass('up-icon');
      $('.icon-sub-menu-invest-menu').removeClass('icon-sub-menu');
      this.current_menu_visible2 = 1;
    }else{
      $('.life_style_menu').attr("style", "display: none !important");
      $('.icon-sub-menu-life-style').removeClass('up-icon');
      $('.icon-sub-menu-invest-menu').addClass('icon-sub-menu');
      this.current_menu_visible2 = 0;
    }
  }
  */

  scrollToID(id, speed) {
    var offSet = 70;
    var obj = $('#' + id);
    localStorage.setItem("redirect_page", id);
    
    if(obj.length){
      var offs = obj.offset();
      var targetOffset = offs.top - offSet;
      $('html,body').animate({ scrollTop: targetOffset }, speed);
    }

    
    $('#mySidenav').css('width','0px');
     this.router.navigate(['/invest']);
    //$('.sub-menu').attr("style", "display: none !important");
  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

   redirect_to_url(url){
    //$(document).find('#back-to-top').click();

    
    //$('.d-block').remove();
    //$('body').removeClass('modal-open');
    //$('body').css('padding-right', '0px');
    //$('.d-block').css('overflow-y', 'auto');
    //alert(url)
    this.router.navigateByUrl(url); 
    setTimeout(function(){
      this.document.location.reload();
    },100);   
    //this.getDismissReason(1);
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
}