import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import Utils from './../shared/utils'
import { HttpClient } from '@angular/common/http';
import { ApiService } from './../services/api.service';
import * as $ from 'jquery';
import { NgwWowService } from 'ngx-wow';

@Component({
  selector: 'app-header',
  templateUrl: './home.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  ImagePath: string; 
  currentYear: number=new Date().getFullYear();
  current_video: any;
  end_point = "http://localhost:3000";
  //end_point = "http://139.59.91.66:3000"
  page_setting: any;
  slides: any;
  loading_img = true;
  current_menu = 1;
  menus: any;
<<<<<<< HEAD
  current_menu_visible = 0;
  current_menu_visible2 = 0;
  
  constructor(public router: Router,private http: HttpClient,public apiurl: ApiService, private wowService: NgwWowService) {
    
=======

  constructor(public router: Router,private http: HttpClient,public apiurl: ApiService, private wowService: NgwWowService) {
>>>>>>> 8984940b6fd992eef6756565fbc19ce2057b00d1
      let api = 'get_settings';
      let formData = new FormData();
      setTimeout(() => this.loading_img = false, this.apiurl.time);
      
      this.http.post(this.apiurl.end_point+`/${api}`, formData)
        .subscribe((response) => {
            if(response){
              this.page_setting = response;
              console.log(response,'response');
              this.wowService.init();
            }
      });

      api = 'get_slider';
      this.http.get(this.apiurl.end_point+`/${api}`)
      .subscribe((response:any) => {
          if(response){
            this.slides = response.pages;
            console.log(response,'response');
            this.wowService.init();
          }
      });

      api = 'get_pages';
      this.http.get(this.apiurl.end_point+`/${api}`)
      .subscribe((response:any) => {
          if(response){
            this.menus = response.pages;
          }
      });
  }

  openNav() {
    document.getElementById("mySidenav").style.width = "250px";
  }

  closeNav() {
    document.getElementById("mySidenav").style.width = "0";
  }  

  ngOnInit() {
      //alert(this.loading)
      let api = 'get_homepage_video';
		  let formData = new FormData();
      var response = {homepage_video:""};
      this.http.post(this.apiurl.end_point+`/${api}`, formData)
	    .subscribe((response) => {
	         if(response){
               this.current_video = response;
               this.wowService.init();
	     	   }
	    });
  }

  menu_navigate(menu){
    //$('html, body').animate({scrollTop:0}, '300');
    this.router.navigate(['/'+menu]);
  }

  externalLink(e,what){
    e.preventDefault();
    window.open(what, '_blank');
  }  


  displayallmenu(e,what,menu_class){
    e.preventDefault();
    console.log($(this),'test');
    if(what==1 && this.current_menu_visible==0){
      $('.'+menu_class).attr("style", "display: block !important");
      $('.'+menu_class).prev().addClass('up-icon');
      this.current_menu_visible = 1;
    }else{
      $('.'+menu_class).attr("style", "display: none !important");
      $('.'+menu_class).prev().removeClass('up-icon');
      this.current_menu_visible = 0;
    }
  }

  /*
  displayallmenu2(e,what){
    e.preventDefault();
    if(what==1 && this.current_menu_visible2==0){
      $('.life_style_menu').attr("style", "display: block !important");
      $('.icon-sub-menu-life-style').addClass('up-icon');
      this.current_menu_visible2 = 1;
    }else{
      $('.life_style_menu').attr("style", "display: none !important");
      $('.icon-sub-menu-life-style').removeClass('up-icon');
      this.current_menu_visible2 = 0;
    }
  }
  */

  scrollToID(id, speed) {
    var offSet = 70;
    localStorage.setItem("redirect_page", id);
    //alert(id)
    var obj = $('#' + id);
    if(obj.length){
      var offs = obj.offset();
      var targetOffset = offs.top - offSet;
      $('html,body').animate({ scrollTop: targetOffset }, speed);
    }

    
    $('#mySidenav').css('width','0px');
    this.router.navigate(['/invest']);
    //$('.sub-menu').attr("style", "display: none !important");
  }
}