import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ApiService } from './../services/api.service';
import * as $ from 'jquery';
import { NgwWowService } from 'ngx-wow';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-home',
  templateUrl: './traveldesign.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./traveldesign.component.css']
})
export class TravelDesignComponent implements OnInit {
  ImagePath: string; 
  currentYear: number=new Date().getFullYear();
  page_data: any;
  page_data2: any;
  //end_point = "http://139.59.91.66:3000"
  end_point = "http://localhost:3000";
  page_setting: any;
  slider_html: any;
  slides: any;
  loading_img = true;
  closeResult: string;
  menus: any;
<<<<<<< HEAD
  current_menu_visible = 0;
  current_menu_visible2 = 0;
  experiance = false;
  culinary = false;
=======
>>>>>>> 8984940b6fd992eef6756565fbc19ce2057b00d1
  
  constructor(private modalService: NgbModal, public router: Router,private http: HttpClient,public apiurl: ApiService, private wowService: NgwWowService) {
    let api = 'get_traveldesign';
    let formData = new FormData();
    setTimeout(() => this.loading_img = false, this.apiurl.time);

		this.http.post(this.apiurl.end_point+`/${api}`, formData)
	    .subscribe((response) => {
          
          this.wowService.init();
        
          if(response){
             this.page_data = response;
             console.log(response,'response');
             
	     	  }
    });

    api = 'get_lifestyle';  
    this.http.post(this.apiurl.end_point+`/${api}`, formData)
      .subscribe((response) => {
          if(response){
            this.page_data2 = response;
          }
    });


    api = 'get_slider';
    var _that = this;
    
    this.http.get(this.apiurl.end_point+`/${api}`)
    .subscribe((response:any) => {

        this.wowService.init();

        if(response){
          _that.slider_html = '';
          this.slides = response.pages;
          response.pages.forEach(function(element,i) {
              _that.slider_html += '<div>';
              _that.slider_html += '<img src="assets/page/'+element.image+'" alt="">';
              _that.slider_html += '<div class="content-wrap"><div class="info-box-title feature-title" style="">'+element.caption+'</div></div>';
              _that.slider_html += '</div>';
          });
        }
    });

    api = 'get_pages';
    this.http.get(this.apiurl.end_point+`/${api}`)
    .subscribe((response:any) => {
        if(response){
          this.menus = response.pages;
<<<<<<< HEAD
          setTimeout(function(){
            $('.eprince-inner').each(function(){           
              let img_src = $(this).find('img.only_desktop_show').attr('src');
              $(this).find('.dfd-heading-delimiter').after($('<img>',{hidden:true,class:'only_mobile_show',src:img_src}))
            });
          },3000);
=======
>>>>>>> 8984940b6fd992eef6756565fbc19ce2057b00d1
        }
    });
  }

  ngOnInit() {
    let api = 'get_settings';
    let formData = new FormData();
    this.http.post(this.apiurl.end_point+`/${api}`, formData)
      .subscribe((response) => {
          
          this.wowService.init();

          if(response){
            this.page_setting = response;
            console.log(response,'response')
          }
    });
  }

  openNav() {
    document.getElementById("mySidenav").style.width = "250px";
  }

  closeNav() {
    document.getElementById("mySidenav").style.width = "0";
  }
  
  menu_navigate(menu){
   // $('html, body').animate({scrollTop:0}, '300');
    this.router.navigate(['/'+menu]);
  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
<<<<<<< HEAD

  externalLink(e,what){
    e.preventDefault();
    window.open(what, '_blank');
  } 

  displayallmenu(e,what,menu_class){
    e.preventDefault();
    console.log($(this),'test');
    if(what==1 && this.current_menu_visible==0){
      $('.'+menu_class).attr("style", "display: block !important");
      $('.'+menu_class).prev().addClass('up-icon');
      this.current_menu_visible = 1;
    }else{
      $('.'+menu_class).attr("style", "display: none !important");
      $('.'+menu_class).prev().removeClass('up-icon');
      this.current_menu_visible = 0;
    }
  } 

  /*

  displayallmenu2(e,what){
    e.preventDefault();
    if(what==1 && this.current_menu_visible2==0){
      $('.life_style_menu').attr("style", "display: block !important");
      $('.icon-sub-menu-life-style').addClass('up-icon');
      this.current_menu_visible2 = 1;
    }else{
      $('.life_style_menu').attr("style", "display: none !important");
      $('.icon-sub-menu-life-style').removeClass('up-icon');
      this.current_menu_visible2 = 0;
    }
  }

  */

  scrollToID(id, speed) {
    var offSet = 70;
    localStorage.setItem("redirect_page", id);
    var obj = $('#' + id);
    if(obj.length){
      var offs = obj.offset();
      var targetOffset = offs.top - offSet;
      $('html,body').animate({ scrollTop: targetOffset }, speed);
    }

    
    $('#mySidenav').css('width','0px');
     this.router.navigate(['/invest']);
   //$('.sub-menu').attr("style", "display: none !important");
  }
=======
>>>>>>> 8984940b6fd992eef6756565fbc19ce2057b00d1
}