import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ApiService } from './../services/api.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as $ from 'jquery';
import { NgwWowService } from 'ngx-wow';
import { range } from 'rxjs';
import * as moment from 'moment';
<<<<<<< HEAD
import {NgbModal, ModalDismissReasons, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
=======
>>>>>>> 8984940b6fd992eef6756565fbc19ce2057b00d1

@Component({
  selector: 'app-home',
  templateUrl: './rentals.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./rentals.component.css']
})
export class RentalsComponent implements OnInit {
  ImagePath: string; 
  currentYear: number=new Date().getFullYear();
  end_point = "http://localhost:3000";
  //end_point = "http://139.59.91.66:3000"
  page_setting: any;
  page_data: any;
  slider_html: any;
  slides:any;
  guesty_api_response: any;
  total_count: any;
  api_search_status: any;
  registerForm: FormGroup;
  today: any;
  next_month: any;
  loading_img = true;
  disabled:boolean;
  dateRangeStart: any;
  dateRangeEnd: any;
  menus: any;
<<<<<<< HEAD
  current_menu_visible = 0;
  current_menu_visible2 = 0;
  hospitality = false;
  lifestyle = false;
  closeResult: string;
  
=======


>>>>>>> 8984940b6fd992eef6756565fbc19ce2057b00d1
  private pickerOptions: Object = {
    //'showDropdowns': true,
    //'showWeekNumbers': true,
    //'timePickerIncrement': 5,
    'autoApply': false,
    //'startDate': '04/27/2016',
    //'endDate': '07/27/2016',
    format:"YYYY-MM-DD",
    displayFormat:"YYYY-MM-DD",
  };
  
  private formats:Array<string> = ['DD-MM-YYYY', 'YYYY/MM/DD', 'DD.MM.YYYY', 'shortDate'];
  private format:string = this.formats[0];
  private startdate: Date = new Date();
  private enddate: Date = new Date();
  private datelabel: string = 'Please choose a date';
  private selected: any;


<<<<<<< HEAD
  constructor(public router: Router,private http: HttpClient,public apiurl: ApiService,private formBuilder: FormBuilder, private wowService: NgwWowService,private modalService: NgbModal) {
=======
  constructor(public router: Router,private http: HttpClient,public apiurl: ApiService,private formBuilder: FormBuilder, private wowService: NgwWowService) {
>>>>>>> 8984940b6fd992eef6756565fbc19ce2057b00d1
    this.disabled = false;
    this.selected = {};
    
    this.attachscript();
<<<<<<< HEAD
=======

>>>>>>> 8984940b6fd992eef6756565fbc19ce2057b00d1
    let api = 'get_settings';
    let formData = new FormData();
    setTimeout(() => this.loading_img = false, this.apiurl.time);

		this.http.post(this.apiurl.end_point+`/${api}`, formData)
	    .subscribe((response) => {
          
          this.wowService.init();
        
          if(response){
             this.page_setting = response;
             console.log(response,'response')
	     	  }
    });

    api = 'get_slider';
    var _that = this;
    
    this.http.get(this.apiurl.end_point+`/${api}`)
    .subscribe((response:any) => {

        this.wowService.init();

        if(response){
          _that.slider_html = '';
          this.slides = response.pages;
          response.pages.forEach(function(element) {
            //console.log(element,'element')
            _that.slider_html += '<div>';
            _that.slider_html += '<img src="assets/page/'+element.image+'" alt="">';
            _that.slider_html += '<div class="content-wrap"><div class="info-box-title feature-title" style="">'+element.caption+'</div></div>';
            _that.slider_html += '</div>';
          });
        }
    });

    api = 'get_pages';
    this.http.get(this.apiurl.end_point+`/${api}`)
    .subscribe((response:any) => {
        if(response){
          this.menus = response.pages;
        }
    });
<<<<<<< HEAD

    setTimeout(function(){
      $('.eprince-inner').each(function(){           
        let img_src = $(this).find('img.only_desktop_show').attr('src');
        $(this).find('.dfd-heading-delimiter').after($('<img>',{hidden:true,class:'only_mobile_show',src:img_src}))
      });
    },3000);
=======
>>>>>>> 8984940b6fd992eef6756565fbc19ce2057b00d1
  }

  dateSelected(message: any) {
    
  }


  attachscript(){
        $('script').each(function() {
          if(this.src.indexOf('pinterest') != -1){
              $(this).remove();
          }
        });
        
        let mainUrl = "https://assets.pinterest.com/js/pinit_main.js"+"?"+Math.random();
        var head = document.getElementsByTagName('head')[0];
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = mainUrl;
        head.appendChild(script);

        setTimeout(function(){
          $('span').each(function(){
            let cls = $(this).attr('class');
            if(cls && cls.indexOf('embed_grid') != -1){
              $( this ).width('1018px');
              $( this ).addClass('set_width');
            }

            if(cls && cls.indexOf('bd') != -1){
              $( this ).height('530px');
              $( this ).addClass('set_height');
            }
          });  
        },4000);
  }

  ngOnInit() {
    let api = 'get_rentals';
		let formData = new FormData();
		this.http.post(this.apiurl.end_point+`/${api}`, formData)
	    .subscribe((response) => {
           
           this.wowService.init(); 
           
           if(response){
             this.page_data = response;
             console.log(response,'response');
             this.wowService.init();
	     	  }
    });

    this.today = new Date();
    var dd = String(this.today.getDate()).padStart(2, '0');
    var mm = String(this.today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = this.today.getFullYear();
    this.today = yyyy + '-' + mm + '-' + dd;

    let curr_date = new Date();
    let curr_date_stmp = curr_date.setDate(curr_date.getDate() + 7 );
    let next_three_day = new Date(curr_date_stmp);
    console.log(next_three_day,'next_three_day')

    let date = new Date();  
    this.next_month = new Date(date.setMonth(date.getMonth()+8));
    var dd = String(next_three_day.getDate()).padStart(2, '0');
    var mm = String(next_three_day.getMonth() + 1).padStart(2, '0'); //January is 0!
    //var yyyy = next_three_day.getFullYear();
    this.next_month = next_three_day.getFullYear() + '-' + mm + '-' + dd;

    this.registerForm = this.formBuilder.group({
      from: [this.today,[]],
      to: [this.next_month,[]],
      guests: ['guests',[]],
      daterangeInput: ['',[]],
    });
  }

  openNav() {
    document.getElementById("mySidenav").style.width = "250px";
  }

  closeNav() {
    document.getElementById("mySidenav").style.width = "0";
  }

  menu_navigate(menu){
   // $('html, body').animate({scrollTop:0}, '300');
    this.router.navigate(['/'+menu]);
  }



  search_data(){
    let api = 'api_guesty';
    this.api_search_status = true;

    const controls = this.registerForm.controls;

    console.log(controls,'controls');

    let formData = new FormData();
    formData.append("to", controls.to.value);
    formData.append("from", controls.from.value);
    formData.append("guests", controls.guests.value);
    //console.log(controls,'controls');

    this.http.post(this.apiurl.end_point+`/${api}`, formData)
	    .subscribe((response:any) => {
	         if(response){
              let res = JSON.parse(JSON.stringify(response));
              //this.guesty_api_response = this.chunks(res.response.results,3);
              this.guesty_api_response = res.response.results;
              this.total_count = res.response.count;
              this.api_search_status = false;
	     	  }
    });
  }

  formatDate(date) {
    console.log(date,'date');

    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year,month,day].join('-');
  }

  selectedDate(event, daterange){
    this.dateRangeStart = this.formatDate(event.start.toString());
    this.dateRangeEnd = this.formatDate(event.end.toString());

    this.registerForm.controls['from'].setValue(this.dateRangeStart);
    this.registerForm.controls['to'].setValue(this.dateRangeEnd);
  }

  chunks(array, size) {
    let results = [];
    results = [];
    while (array.length) {
        results.push(array.splice(0, size));
    }
    return results;
  };

  externalLink(e,what){
    e.preventDefault();
    window.open(what, '_blank');
  }  


  displayallmenu(e,what,menu_class){
    e.preventDefault();
    console.log($(this),'test');
    if(what==1 && this.current_menu_visible==0){
      $('.'+menu_class).attr("style", "display: block !important");
      $('.'+menu_class).prev().addClass('up-icon');
      this.current_menu_visible = 1;
    }else{
      $('.'+menu_class).attr("style", "display: none !important");
      $('.'+menu_class).prev().removeClass('up-icon');
      this.current_menu_visible = 0;
    }
  }


  /* 
  displayallmenu2(e,what){
    e.preventDefault();
    if(what==1 && this.current_menu_visible2==0){
      $('.life_style_menu').attr("style", "display: block !important");
      $('.icon-sub-menu-life-style').addClass('up-icon');
      this.current_menu_visible2 = 1;
    }else{
      $('.life_style_menu').attr("style", "display: none !important");
      $('.icon-sub-menu-life-style').removeClass('up-icon');
      this.current_menu_visible2 = 0;
    }
  }
  */

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  scrollToID(id, speed) {
    var offSet = 70;
    localStorage.setItem("redirect_page", id);
    
    var obj = $('#' + id);
    if(obj.length){
      var offs = obj.offset();
      var targetOffset = offs.top - offSet;
      $('html,body').animate({ scrollTop: targetOffset }, speed);
    }

    
    $('#mySidenav').css('width','0px');
     this.router.navigate(['/invest']);
    //$('.sub-menu').attr("style", "display: none !important");
  }
}