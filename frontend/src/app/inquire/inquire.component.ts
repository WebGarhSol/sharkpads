import { Component, OnInit, ViewEncapsulation, Pipe, PipeTransform  } from '@angular/core';
import {ElementRef, ViewChild,Inject } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { DomSanitizer, SafeHtml, SafeStyle, SafeScript, SafeUrl, SafeResourceUrl } from '@angular/platform-browser';
import Utils from './../shared/utils'
import { ApiService } from './../services/api.service';
import * as $ from 'jquery';
import { NgwWowService } from 'ngx-wow';
<<<<<<< HEAD
import { DOCUMENT } from '@angular/common';
=======
>>>>>>> 8984940b6fd992eef6756565fbc19ce2057b00d1

@Component({
  selector: 'app-home',
  templateUrl: './inquire.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./inquire.component.css']
})

@Pipe({
  name: 'safe'
})


export class InquireComponent implements OnInit {
  @ViewChild('myDiv') myDiv: ElementRef<HTMLElement>;

  ImagePath: string; 
  currentYear: number=new Date().getFullYear();
  registerForm: FormGroup;
  submitted = false;
  form_validation_msg = '';
  end_point = "http://localhost:3000";
  //end_point = "http://139.59.91.66:3000"
  page_setting: any;
  slides: any;
  slider_html: any;
  map:any;
  loading_img = true;
  menus: any;
<<<<<<< HEAD
  current_menu_visible = 0;
  current_menu_visible2 = 0;

  constructor(@Inject(DOCUMENT) private document: Document,public router: Router,private formBuilder: FormBuilder,private http: HttpClient,protected _sanitizer: DomSanitizer,public apiurl: ApiService, private wowService: NgwWowService) {
=======

  constructor(public router: Router,private formBuilder: FormBuilder,private http: HttpClient,protected _sanitizer: DomSanitizer,public apiurl: ApiService, private wowService: NgwWowService) {
>>>>>>> 8984940b6fd992eef6756565fbc19ce2057b00d1
  	let api = 'get_settings';
    let formData = new FormData();
    setTimeout(() => this.loading_img = false, this.apiurl.time);

		this.http.post(this.apiurl.end_point+`/${api}`, formData)
	    .subscribe((response:any) => {

           this.wowService.init();

	         if(response){
             this.page_setting = response;
             this.map = this._sanitizer.bypassSecurityTrustResourceUrl(response.traveldesign.map);
             console.log(response,'response')
	     	  }
    });

    api = 'get_slider';
    var _that = this;

    this.http.get(this.apiurl.end_point+`/${api}`)
    .subscribe((response:any) => {
        if(response){
           _that.slider_html = '';
          this.slides = response.pages;
          
          this.wowService.init();

          response.pages.forEach(function(element) {
            //console.log(element,'element')
            _that.slider_html += '<div>';
            _that.slider_html += '<a routerLink="'+element.link+'">';
            _that.slider_html += '<img src="assets/page/'+element.image+'" alt="">';
            _that.slider_html += '<div class="content-wrap"><div class="info-box-title feature-title" style="">'+element.caption+'</div></div>';
            _that.slider_html += '</a>';
            _that.slider_html += '</div>';
          });
        }
    });

    api = 'get_pages';
    this.http.get(this.apiurl.end_point+`/${api}`)
    .subscribe((response:any) => {
        if(response){
          this.menus = response.pages;
        }
    });
  }

  openNav() {
    document.getElementById("mySidenav").style.width = "250px";
  }

  closeNav() {
    document.getElementById("mySidenav").style.width = "0";
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      phone: [''],
      comment: [''],
      subject: ['']
    });
  }

  get f() { return this.registerForm.controls; }

  onSubmit() {
    
    this.submitted = true;
    // stop here if form is invalid
    // console.log(this.registerForm.FormGroup.controls.name.errors.required,'test');
    //alert('sssssss')
    //console.log(this.registerForm,'ddddddddddddd')
    if (this.registerForm.invalid) {
      return;
    }

    alert('Mail sent successfully');
    
    const controls = this.registerForm.controls;
    
    let formData = new FormData();
    formData.append("name", controls.name.value);
	  formData.append("email", controls.email.value);
    formData.append("phone",controls.phone.value);
    formData.append("comment",controls.comment.value);
    formData.append("subject",controls.subject.value);

    //alert('sssssssvvvvvvvvvvvvvv')

    let api = 'api_contact_us';
	  this.http.post(this.apiurl.end_point+`/${api}`, formData)
	    .subscribe((response:any) => {
	        if(response){
            this.submitted = false;
            //alert(response.msg);
            this.registerForm.reset();
            //console.log(response,'response');
	     	  }
	    });
  }

  onReset() {
      this.submitted = false;
      this.registerForm.reset();
  }

  menu_navigate(menu){
    //$('html, body').animate({scrollTop:0}, '300');
    this.router.navigate(['/'+menu]);
  }

  externalLink(e,what){
    e.preventDefault();
    window.open(what, '_blank');
  }  

  
  displayallmenu(e,what,menu_class){
    e.preventDefault();
    console.log($(this),'test');
    if(what==1 && this.current_menu_visible==0){
      $('.'+menu_class).attr("style", "display: block !important");
      $('.'+menu_class).prev().addClass('up-icon');
      this.current_menu_visible = 1;
    }else{
      $('.'+menu_class).attr("style", "display: none !important");
      $('.'+menu_class).prev().removeClass('up-icon');
      this.current_menu_visible = 0;
    }
  }
  /*
  

  displayallmenu2(e,what){
    e.preventDefault();
    if(what==1 && this.current_menu_visible2==0){
      $('.life_style_menu').attr("style", "display: block !important");
      $('.icon-sub-menu-life-style').addClass('up-icon');
      this.current_menu_visible2 = 1;
    }else{
      $('.life_style_menu').attr("style", "display: none !important");
      $('.icon-sub-menu-life-style').removeClass('up-icon');
      this.current_menu_visible2 = 0;
    }
  }
  */

  scrollToID(id, speed) {
    var offSet = 70;
    localStorage.setItem("redirect_page", id);
    var obj = $('#' + id);
    if(obj.length){
      var offs = obj.offset();
      var targetOffset = offs.top - offSet;
      $('html,body').animate({ scrollTop: targetOffset }, speed);
    }

    
    $('#mySidenav').css('width','0px');
     this.router.navigate(['/invest']);
    //$('.sub-menu').attr("style", "display: none !important");
  }
}